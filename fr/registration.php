<?php
session_start();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../functPHP/PHPMailer-master/src/Exception.php';
require '../functPHP/PHPMailer-master/src/PHPMailer.php';
require '../functPHP/PHPMailer-master/src/SMTP.php';
require "./src/help.php";

// empecher le spam formulaire
if (isset($_SESSION['registration']) AND $_SESSION['registration'] == "inscrit"){
 $_SESSION['registration'] = "autorise" ;
  goToIndex();
}
$_SESSION['registration'] = "autorise";

$count =0;
$bdd= get_pdo();
$requete =$bdd->prepare('SELECT email from administrateur');
$requeteisok = $requete->execute();
foreach ($requete as $value) {
    $emailenv = $value['email'];
}
$requetecount =$bdd->prepare('SELECT * FROM formation');
$requetecntisok = $requetecount->execute();
if (!isset($_GET['idform']) OR !is_numeric($_GET['idform']) ) {
    e404();
}

$_SESSION['idform'] = isset($_SESSION['idform']) ? $_SESSION['idform'] : $_GET['idform'];


$idF = $_GET['idform'];

$requetID = $bdd->prepare("SELECT * FROM formation where id_form = '$idF' ");
$okreq = $requetID ->execute();
$nameF = $requetID->fetch();
// récupération du nom de la formation
if ($nameF) {
    $nomForma = $nameF['nom'];
}else{
    e404();
}


$exec1 = isset($exec1) ? $exec1 : 0;
$exec2 = isset($exec2) ? $exec2 : 0;
$exec3 = isset($exec3) ? $exec3 : 0;

if(isset($_POST['Envoie']) AND !empty($_POST['Envoie'])){
    // en cas de bot
  if (isset($_POST['honey']) AND !empty($_POST['honey'])){
    goToIndex();
    exit;
  }
    $message = "";
    $message2 = "";
    $message3 = "";
    if (preg_match("#^[0-9]{10}$#i", htmlspecialchars($_POST['Tel'])) || preg_match("#^[0-9]{0}$#i", htmlspecialchars($_POST['Tel']))) {
            $exec1 =1;
    }  else{
        $message = "Le téléphone";
    }
    if (preg_match("#^[0-9]{5}$#i", htmlspecialchars($_POST['adresseP']))) {
            $exec2 = 1;  
    } else{
        $message2 = ", l'adresse postale ";
    }
    if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i", htmlspecialchars($_POST['adresseM']))){
            $exec3 = 1;
    }else{
        $message3 = ", l'adresse mail ";
    }

    if ($exec1 == 1 && $exec2 == 1 && $exec3 == 1) {
    $mail = new PHPMailer(true);    

    try {
        //Server settings
        $mail->SMTPDebug = 0;                      
        $mail->isSMTP();                                            
        $mail->Host       = 'tls://smtp.gmail.com';                    
        $mail->SMTPAuth   = true;                                   
        $mail->Username   = 'dg8125791@gmail.com';                     
        $mail->Password   = 'maile1234';                               
        $mail->SMTPSecure = 'tls';         
        $mail->Port       = 587;                                    
        $prenom = htmlspecialchars($_POST['prenom']);
        $nom = htmlspecialchars($_POST['nom']);
        $adresseM = htmlspecialchars($_POST['adresseM']);
        if (empty(htmlspecialchars($_POST['Tel']))) {
        $_POST['Tel'] = "UNDEFINE";
        }
        $Tel = htmlspecialchars($_POST['Tel']);
        $adres = htmlspecialchars($_POST['adresse']);
        $adresseP = htmlspecialchars($_POST['adresseP']);
        $ville = htmlspecialchars($_POST['vil']);
        try {
        $requestcompt = $bdd->query("SELECT COUNT(*) AS nb FROM client");
        $requestcompt ->execute();
        $s=$requestcompt->fetch(PDO::FETCH_OBJ);
        $nbrl= intval($s->nb +1);
        $id = $_SESSION['idform'];
     

        $req = $bdd->prepare('INSERT INTO client VALUES("$nbrl",:nom,:prenom,:email,:telephone,:adresse,:ville,:cp,:idformation)');
        $reqIsOk = $req->execute(array(
          'nom' => $nom,
          'prenom' => $prenom,
          'email'=> $adresseM,
          'telephone' => $Tel,
          'adresse' => $adres,
          'ville' =>$ville,
          'cp' => $adresseP,
          'idformation' => $id
        ));



	  	if ($reqIsOk){
	  		status("Merci pour votre inscription. Un mail de confirmation vous sera envoyé prochainement.");
            $_SESSION['registration'] = "inscrit";
            header("Refresh:5; url =/");
	  	}else{
	  		status("Votre inscription n'a pas pu être enregistrée..., 'error' ");
	  	}

      } catch (Exception $e) {
        echo($e->getMessage());
      }

    //Recipients
     $mail->setFrom('vincentducournau987654321@gmail.com',  $prenom .' s\'est inscrit a la formation '. $nomForma . ' !' );
    $mail->addAddress($emailenv, 'Reception des inscriptions aux formations - La Dépêche du Syndic');     
    // $mail->addReplyTo('info@example.com', 'Information');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    // Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    
    $subject ='Nouvelle inscription pour '.$prenom.' '.$nom;
    $message ='<h2>'.$prenom.' '.$nom.' s\'est inscrit à la formation : '. $nomForma .'.</h2><br>'.
    'Plus d\'infos à propos de '.$prenom.' :<br>'.
    '<b>Adresse</b> : '.$adres.'.<br>'.
    '<b>CP</b> : '.$adresseP.'. <br>'.
    '<b>Ville</b>  : '.$ville.'. <br>'.
    '<b>Teléphone</b> : '.$Tel.' <br>'.
    '<b>Email</b> : '.$adresseM.'.<br>'.
    
    
    // Content
    $mail->isHTML(true);                                  
    $mail->Subject = $subject;
    $mail->Body    = $message;
    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    

} catch (Exception $e) {
    status("L'email n'a pas pu être envoyé", 'error' );
}
}else {
    if ($exec1 ==1 && $exec2 ==1 || $exec2 ==1 && $exec3 ==1 || $exec1 ==1 && $exec3 ==1 || $exec1 ==1 && $exec2 ==1 && $exec3==1) {
        if ($exec1 == 0) {
            $message = "Le téléphone ";
        }
        if ($exec2 == 0) {
            $message2 = "L'adresse postale ";
        }
        if ($exec3 == 0) {
            $message3 = "L'adresse mail ";
        }
        status($message.$message2.$message3." est invalide", 'error' );
    }else{
        if ($exec1 == 1) {
            $message2 = "L'adresse postale ";
        }
    status($message.$message2.$message3." sont invalides", 'error' );
    }
}
        }
// ne pas oublier de désactiver la sécurité sur le compte google

include '../include/header.inc'; ?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css"  href="../CSS/registration.css">
    <link type="text/css" rel="text/css" href="../CSS/font_style.css">
    <link rel="Stylesheet" type="text/css" href="../CSS/variable_style.css"/>
    <link rel="stylesheet" type="text/css" href="../CSS/responsive.css" >
</head>
<body>
<h1 class="t-white centrer disp">Formulaire d'inscription</h1>   
<div class="form-formation">
    <form  method="POST">
        <label>Prénom *</label>
        <input type="text" name="prenom" id="prenom" class="PartF" placeholder="Votre prénom..." required>
        <label>Nom *</label>
        <input type="text" name="nom" id="nom" class="PartF" placeholder="Votre nom..." required >
        <label>E-mail *</label>
        <input type="text" name="adresseM" id="adresseM" class="PartF" placeholder="jeminscris@domaine.com" required>
        <label>Télephone</label>
        <input type="text" name="Tel" id="Tel" class="PartF">
        <label>Code postal *</label>
        <input type="text" name="adresseP" id="adresseP" placeholder="ex : 31000" class="PartF" required>
        <label>Adresse *</label>
        <input type="text" name="adresse" id="adresse" placeholder="ex : 31 rue des pâquerettes" class="PartF" required>
        <label>Ville *</label>
        <input type="text" name="vil" id="vil" placeholder="ex : Toulouse" class="PartF" required>
        <label><strong>Formation choisie</strong></label>
        <input type="text" disabled="disabled" value="<?= $nameF['nom']?>">
        <input type="text*" name="honey" id="honey">
        <br>
        <button class="submit_formulaire"  type="submit" value="Envoie" name="Envoie">Je m'inscris !</button>
    </form>
</div>

</body>
</html>

<?php
include '../include/footer.inc';
?>
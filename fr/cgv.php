<?php 
include '../include/header.inc';

?>

<!DOCTYPE html>
<html>
<head>
	<title>Condition Générales de Vente</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
	<h2 class="centrer">Conditions Générales de <strong>Vente</strong></h2>

	<h3 class="decaleg">DÉFINITIONS</h3>
	<div class="decaleg">
		<p><em>Client :</em> co-contractant de la société LA DEPECHE DU SYNDIC.</p> <br>
		<p><em>Formations inter-entreprises :</em> formations regroupant des stagiaires d’entreprises différentes et réalisées dans des locaux loués par la société LA DEPECHE DU SYNDIC ou mis à disposition par l’une des sociétés clientes.</p> <br>
		<p><em>Formations intra-entreprises :</em> Formations intra-entreprises : formations sur mesure réalisées pour le compte d’un ou plusieurs clients dans des locaux mis à disposition par l’une des sociétés clientes ou loués par la société LA DEPECHE DU SYNDIC.</p> <br>
	</div>
	<h3 class="decaleg">OBJET ET CHAMPS D’APPLICATION</h3>
	<div class="decaleg">
		<p>Les conditions générales de vente décrites ci-après détaillent les droits et obligations de la société LA DEPECHE DU SYNDIC et de son client dans le cadre de la vente de formations professionnelles. Toute prestation accomplie par la société LA DEPECHE DU SYNDIC implique donc l’adhésion sans réserve du client aux présentes conditions générales de vente.</p> <br>
	</div>
	<h3 class="decaleg">DOCUMENTS CONTRACTUELS</h3>
	<div class="decaleg">
		<p>La société LA DEPECHE DU SYNDIC fait parvenir au client une convention ou un contrat de formation professionnelle établie selon les dispositions de la sixième partie du Code du Travail portant organisation de la formation professionnelle tout au long de la vie.</p> <br>
	</div>
	<h3 class="decaleg">CONDITIONS FINANCIÈRES</h3>
	<div class="decaleg">
		<p>Les prix des prestations vendues sont ceux en vigueur au jour de la signature du contrat ou de la convention. Ils sont libellés en euros et calculés hors taxes. Par voie de conséquence, ils seront majorés du taux de TVA au jour de la commande.</p> <br>
		<p>Ces tarifs comprennent la formation, sa préparation et les supports pédagogiques. Ils comprennent également les frais de location de salle dans le cas des formations inter-entreprises. Ils ne comprennent pas les frais de location de salle pour les formations intra-entreprises et les formations individuelles, de déplacement, d’hébergement ou de restauration.</p> <br>
		<p>La société LA DEPECHE DU SYNDIC s’accorde le droit de modifier ses tarifs à tout moment. Toutefois, elle s’engage à facturer les formations commandées aux prix indiqués lors de l’enregistrement de la commande.</p> <br>
	</div>
	<h3 class="decaleg">ESCOMPTE</h3>
	<div class="decaleg">
		<p>Aucun escompte ne sera consenti en cas de paiement anticipé.</p> <br>
	</div>
	<h3 class="decaleg">MODALITÉS DE PAIEMENT</h3>
	<div class="decaleg">
		<p>Le règlement se fait par virement ou chèque bancaire à l’ordre de LA DEPECHE DU SYNDIC. Lors de l’enregistrement de la commande, le client devra verser un acompte de 30% du montant global de la facture, le solde devant être payé à l’issue de la formation à réception de la facture.</p> <br>
	</div>
	<h3 class="decaleg">RETARD DE PAIEMENT</h3>
	<div class="decaleg">
		<p>En cas de défaut de paiement total ou partiel des formations, le client doit verser à la société LA DEPECHE DU SYNDIC une pénalité de retard égale à trois fois le taux de l’intérêt légal. Le taux de l’intérêt légal retenu est celui en vigueur au dernier jour de la formation.</p> <br>
		<p>A compter du 1er janvier 2015, le taux d’intérêt légal sera révisé tous les 6 mois (Ordonnance n°2014-947 du 20 août 2014). Cette pénalité est calculée sur le montant TTC de la somme restant due, et court à compter de la date d’échéance du prix sans qu’aucune mise en demeure préalable ne soit nécessaire. En sus des indemnités de retard, toute somme, y compris l’acompte, non payée à sa date d’exigibilité produira de plein droit le paiement d’une indemnité forfaitaire de 40 euros due au titre des frais de recouvrement. Articles 441-6, I alinéa 12 et D. 441-5 du code de commerce.</p> <br>
	</div>
	<h3 class="decaleg">CLAUSE RÉSOLUTOIRE</h3>
	<div class="decaleg">
		<p>Si dans les quinze jours qui suivent la mise en œuvre de la clause ” Retard de paiement “, le client ne s’est pas acquitté des sommes restant dues, la vente sera résolue de plein droit et pourra ouvrir droit à l’allocation de dommages et intérêts au profit de la société LA DEPECHE DU SYNDIC.</p> <br>
	</div>
	
	<h3 class="decaleg">DÉDIT OU ABANDON</h3>
	<div class="decaleg">
		<p><em>Toute annulation par le client doit être communiquée par écrit.</em></p> <br>
		<p>En cas de résiliation de la convention ou du contrat de formation par le client à moins de 90 jours francs avant le début d’une des actions mentionnées dans la convention ou le contrat, la société LA DEPECHE DU SYNDIC retiendra sur le coût total un pourcentage de 30%, au titre de dédommagement.</p> <br>
		<p>En cas de réalisation partielle de l’action du fait du client, seule sera facturée au client la partie effectivement réalisée de l’action selon le prorata suivant : [nombre d’heures réalisées / nombre d’heures prévues]. En outre la société LA DEPECHE DU SYNDIC retiendra sur le coût correspondant à la partie non réalisée de l’action un pourcentage de 30% au titre de dédommagement.</p> <br>
		<p>Les montants versés par le client au titre de dédommagement ne pourront pas être imputés par le client sur son obligation définie à l’article L6331-1 du code du travail ni faire l’objet d’une demande de remboursement ou de prise en charge par un OPCO.</p> <br>
		<p>Pour les formations interentreprises, dans le cas où le nombre minimum de 4 stagiaires ne serait pas atteint, la société LA DEPECHE DU SYNDIC se réserve la possibilité de reporter l’action de formation à une date ultérieure ou de l’annuler. En cas d’annulation de l’action de formation par la société LA DEPECHE DU SYNDIC, l’acompte de 30% sera intégralement restitué au client.</p> <br>
	</div>
	<h3 class="decaleg"> RÈGLEMENT PAR UN OPCO</h3>
	<div class="decaleg">
		<p>Si le client souhaite que le règlement soit émis par l’OPCO dont il dépend, il lui appartient :</p> <br>
		<ul class="decaleg">
			<li>de faire une demande de prise en charge avant le début de la formation et de s’assurer de la bonne fin de cette demande</li>
			<li>de l’indiquer explicitement par écrit avant la rédaction de la convention ou du contrat</li>
			<li>de s’assurer de la bonne fin du paiement par l’OPCO qu’il aura désigné</li>
		</ul> <br>
		<p>Si l’OPCO ne prend en charge que partiellement le coût de la formation, le reliquat sera facturé au client. Si LA DEPECHE DU SYNDIC n’a pas reçu la prise en charge de l’OPCO au 1er jour de la formation, le client sera facturé en intégralité du coût de la formation</p> <br>
		<p>En cas de non paiement par l’OPCO pour quelque motif que ce soit, le Client sera redevable de l’intégralité du coût de la formation et sera facturé du montant correspondant. </p> <br>
	</div>
	<h3 class="decaleg">COMMUNICATION</h3>
	<div class="decaleg">
		<p>A l’issue de la formation, le client personne morale autorise la société LA DEPECHE DU SYNDIC à communiquer le logo de son entreprise ainsi que les témoignages des stagiaires sur les sites Internet de la société LA DEPECHE DU SYNDIC s’ils y ont eux mêmes préalablement et expressément consenti.</p> <br>
	</div>
	<h3 class="decaleg"> POLITIQUE DE CONFIDENTIALITÉ</h3>
	<div class="decaleg">
		<p>LA DEPECHE DU SYNDIC est tenue de collecter des données à caractère personnel relatives notamment à l’identification de la personne, ses coordonnées, sa situation professionnelle, son profil.</p> <br>
		<p>Ces données sont destinées à la réalisation d’opérations de formation :</p> <br>
		<ul class="decaleg">
			<li>prospection commerciale (prise de contact, recherche de nouveaux clients ou formateurs) ;</li>
			<li>exécution de la prestation (édition des conventions ou contrats, animations, évaluations, communications et tout autre acte y afférent) ;</li>
			<li>réponse aux obligations légales, règlementaires, comptables et administratives en vigueur ;</li>
		</ul> <br>
		<p>Le traitement de ces données est obligatoire pour l’accomplissement de ces finalités. A défaut, LA DEPECHE DU SYNDIC pourrait se trouver dans l’incapacité d’exécuter son activité et, notamment, tout ou partie des missions qui lui sont confiées.</p> <br>
		<p>LA DEPECHE DU SYNDIC collecte uniquement les données nécessaires à la finalité du traitement envisagé et ne communique que celles qui sont nécessaires aux prestataires et tiers concernés par l’opération.</p> <br>
		<p>Ces données sont conservées que pour une certaine durée, à savoir :</p> <br>
		<ul class="decaleg">
			<li>3 (trois) ans à compter du dernier contact dans le cadre de la prospection ;</li>
			<li>10 (dix) ans en principe à compter de la fin de la relation contractuelle ;</li>
			<li>l’ensemble de ces données peut toutefois être conservé plus longtemps que les durées précitées après obtention de votre accord, sous forme d’archives, pour répondre aux obligations légales et réglementaires éventuelles qui s’imposent ou encore pendant d’autres délais légaux de prescription applicables.</li>
		</ul> <br>
		<p>Toute personne physique concernée dispose de droits d’accès, rectification, suppression, limitation et d’opposition qu’elle peut exercer à tout moment en écrivant par courriel à : viviane.labbe@ladepechedusyndic.fr ou courrier postal à l’adresse suivante : LA DEPECHE DU SYNDIC – 1015 avenue du Lauragais – 31 860 LABARTHE SUR LEZE en joignant une copie de sa carte d’identité</p> <br>
		<p>En cas de désaccord, la CNIL peut être saisie à partir de son site internet : <a href="http://www.cnil.fr">www.cnil.fr</a></p> <br>
		<p>En cas de collecte des coordonnées téléphoniques d’un consommateur, LA DEPECHE DU SYNDIC informe les consommateurs de l’existence de la liste d’opposition au démarchage téléphonique « Bloctel », sur laquelle ils peuvent s’inscrire (https://conso.bloctel.fr/).</p> <br>
	</div>
	<h3 class="decaleg">TRIBUNAL COMPÉTENT</h3>
	<div class="decaleg">
		<p>Tout litige relatif à l’interprétation et à l’exécution des présentes conditions générales de vente est soumis au droit français. À défaut de résolution amiable, le litige sera porté devant le Tribunal du lieu du siège social de la société LA DEPECHE DU SYNDIC.</p> <br>
	</div>

	

</body>
</html>


<?php 
include '../include/footer.inc';

?>
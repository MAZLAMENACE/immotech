<?php
include '../include/header.inc';
require_once 'blog/function.php';
require_once '../fr/src/help.php';
$articles = getArticles();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="../CSS/font_style.css" />
  <link rel="stylesheet" href="../CSS/variables_style.css" />
  <link rel="stylesheet" href="../CSS/responsive.css" />
  <link rel="stylesheet" href="../CSS/actualite.css" />
  <title>Actualités</title>
</head>

<body>
  <div id="container_entete_actu">
    <div id="titre_entete_actu">
      <h1 class="t-white">
        Les actualités
      </h1>
    </div>
  </div>
  <div id="corps_actu">
    <?php if (isset($articles) AND !empty($articles)): ?> 
    <?php foreach ($articles as $article) : ?>
     <div class="actualite spot2">
      <div class="actu">
        <h1 class="decaleg"><?= $article->titre?></h1>
        <time class="decaleg"><?php date_convert("Publié le", $article->dateajout); ?></time>
        <br><p class="disp"><?= strip_tags(substr($article->contenu, 0, 500 )."..."); ?>
        <a href="article.php?id=<?= $article->id ?>">Lire la suite</a>
      </div>
        <img src="admin/upload/<?= $article->image?>" alt="" class="tailleimg"/>
    </div>
    <?php endforeach ?>
    <?php else : ?>
      <div class="section">
        <p class=" disp">Aucun article n'a été ajouté pour le moment.</p>
      </div>
    <?php endif ?>
  </div>
</body>
</html>
<script src="https://unpkg.com/scrollreveal"></script>
<script type="text/javascript" src="../JS/interaction.js"></script>

<?php
include '../include/footer.inc';
?>
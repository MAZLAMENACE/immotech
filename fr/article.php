<?php 
require_once 'blog/function.php';
require_once '../fr/src/help.php';


// Vérification de la méthode get
if (!isset($_GET['id']) OR !is_numeric($_GET['id'])){
	require_once 'src/help.php';
	e404();
	}
else{
	extract($_GET);
	$id = strip_tags($id);
	require_once "blog/function.php";
	$article = getArticle($id);
}
include_once 'src/help.php';
include '../include/header.inc';

// AFFICHAGE DES ARTICLES RECENTS
$bdd = get_pdo();
$giveArticles = $bdd->prepare('SELECT * FROM news ORDER BY dateajout DESC');
$giveArticles->execute();
$articlesTitle = $giveArticles->fetchAll();


?>
<!DOCTYPE html>
<html>
<head>
	<title><?= $article->titre?></title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
  	<link rel="stylesheet" href="../CSS/variables_style.css" />
	<link rel="stylesheet" href="../CSS/font_style.css" />
	<link rel="stylesheet" href="../CSS/article.css">
</head>
<body>
	
	<div id="article-contain">
			<div class="header-article">	
				<h2 class="big-title"><?= $article->titre?></h2>
				<time><?php date_convert("Publié le", $article->dateajout)?></time><br><br>
			</div>
			<div class="contenu_article">
			<img src="admin/upload/<?= $article->image?>" alt="" class="tailleimg"/>
			<p id="corps-blog"><?=$article->contenu?></p><br>
			
		</div>
		<br>
		<span class="separate-article"></span>
		<br>
			<h2 class="centrer ent">Articles <strong>récents</strong></h2>
		<div class="article-recent">
			<?php foreach($articlesTitle as $articleTitle): ?>
				<div class="box-article-recent">
					<a class="normal-a " href="article?id=<?= $articleTitle['id'] ?>">
						<div>
							<img class="img-actu" src="admin/upload/<?=$articleTitle['image']?>">
						</div>
						<div>
							<p class="title-article-nav"><?= $articleTitle['titre']?></p>
					</a><br>
						</div>
				</div>
			<?php endforeach ?> 
		</div>
	</div>


</body>
</html>

<?php 

include '../include/footer.inc';


?>

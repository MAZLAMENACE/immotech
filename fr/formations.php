<?php
require "src/help.php";
include_once '../include/header.inc';
$bdd = get_pdo();

$_SESSION["idformation"] = null;

//Prend toutes les colonnes de la table formation pour l'afficher entièrement dans la page initialement
$queryType = 'SELECT * FROM formation WHERE now() < start_formation order by id_form DESC';
$tab_form = $bdd->prepare($queryType);
$tab_form->execute();
$AllfromFormation = $tab_form->fetchAll(PDO::FETCH_ASSOC);

//Prend tout les types de formations pour le selecteur de gauche
$queryType = 'SELECT DISTINCT type FROM formation WHERE now() < start_formation order by id_form DESC';
$tab_form = $bdd->prepare($queryType);
$tab_form->execute();
$Type = $tab_form->fetchAll(PDO::FETCH_ASSOC);

//Prend toutes les villes pour le selecteur de droite
$queryVille = 'SELECT DISTINCT ville FROM formation';
if (isset($_POST["Formation"])) {                                                      //le but de ce bloc d'instructions et de filtrer directement 
    $queryVille .= ' WHERE type = :type';                                              //dans le selecteur des villes après selection d'une formation
    $tab_form = $bdd->prepare($queryVille);
    $tab_form->bindValue(':type', $_POST["Formation"]);
    $tab_form->execute();
    $Ville = $tab_form->fetchAll(PDO::FETCH_ASSOC);
} else {
    $queryVille .= ' WHERE now() < start_formation order by id_form DESC';
    $tab_form = $bdd->prepare($queryVille);
    $tab_form->execute();
    $Ville = $tab_form->fetchAll(PDO::FETCH_ASSOC);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../CSS/formation.css" />
    <link rel="stylesheet" href="../CSS/responsive.css" />
    <link rel="stylesheet" href="../CSS/font_style.css" />
    <link rel="stylesheet" href="../CSS/variables_style.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/index.css">
    <link rel="stylesheet" type="text/css" href="../CSS/buttons.css" />
    <title>Formations</title>
</head>
<body>
    <div id="container_entete_forma">
        <div id="titre_entete_forma">
            <h1 class="t-white">
                Les formations
            </h1>
        </div>
    </div>
    <h2 class="centrer ent">Conseil et <strong>accompagnement</strong></h2>
    <div class="conseils">
        <div class="box-conseil delay">
            <img src="../annex/images/formation/assurance.svg">
            <h3 class="no_p">Vous souhaitez vous assurer de la bonne mise en œuvre des obligations liées au métier de syndic ?</h3><br>
             <br><p><span class="puce-conseil">&#x2192 </span>Je réalise un audit approfondi de vos portefeuilles.</p>
             <br><p><span class="puce-conseil">&#x2192 </span> Et vous propose la mise en place de méthodes et process visant à l’amélioration et au développement de votre activité.</p>
        </div>
        <div class="box-conseil delay">
            <img src="../annex/images/formation/gestion.svg">
             <h3 class="no_p">Vous avez besoin de vous assurer de la bonne mise à jour des données de vos immeubles sans surcharger vos équipes ?</h3><br>
             <br><p><span class="puce-conseil">&#x2192</span> Je mets en place du dossier d’identification de chaque immeuble.</p>
            <br><p><span class="puce-conseil">&#x2192</span> Et vous assiste dans la gestion de vos dossiers tels que l’immatriculation des copropriétés.</p>
        </div>
    </div>
    <h2 class="centrer ent">Mes <strong>formations</strong></h2>
    <div class="intro-formation translate">
        <div class="box-intro-formation-l">
           <!--  <img src="../annex/images/accueil/accueil1.jpg" alt=""> -->
        </div>
        <div class="box-intro-formation-r"> 
            <p class="bold">Vous accueillez des gestionnaires n’ayant que peu d’expérience du terrain et/ou développez vos portefeuilles par la rentrée de nouveaux mandats ?</p> <br>
             <p><span class="puce-conseil">&#x2192</span> Je vous propose un parcours complet d’intégration : tous les outils pour une prise en main rapide d’un nouveau portefeuille.</p>
            <br> <p class="bold">Vous envisagez une mise à niveau des connaissances de vos collaborateurs ?</p><br>
           <p><span class="puce-conseil">&#x2192</span> Notre catalogue présente un ensemble de formations sur les nouveaux enjeux de la gestion immobilière, en blended learning (présentiel et distanciel) avec mises en pratique et observations terrain.</p>
        </div> 
    </div>
    <h2 class="centrer ent">Mes <strong>programmes</strong></h2>
    <div class="prestations">
        <div class="box-prestation delay">
            <a href="../annex/doc/Mieux-organiser-son-activité-immobilière.pdf" onclick="window.open(this.href); return false;" class="normal-a centrer">
                <img src="../annex/images/formation/organisation.svg" alt="" class="go-returned">
            </a>
            <div class="label-prestation">
                <p>Organiser son activité immobilière</p>
        </div>
        </div>
        <div class="box-prestation delay">
            <a href="../annex/doc/ELAN-en-copropriété.pdf" onclick="window.open(this.href); return false;" class="normal-a centrer">
                <img src="../annex/images/formation/law.svg" alt="" class="go-returned">
            </a>
            <div class="label-prestation">
                <p>La loi ELAN</p>
        </div>
        </div>
        <div class="box-prestation delay">
            <a href="../annex/doc/Prévenir-et-gérer-les-conflits.pdf" onclick="window.open(this.href); return false;" class="normal-a centrer">
                <img src="../annex/images/formation/conflict.svg" alt="" class="go-returned">
            </a>
            <div class="label-prestation">
                <p>Prévenir et gérer les conflicts</p>
        </div>
        </div>
        <div class="box-prestation delay">
            <a href="../annex/doc/conformité-RGPD-programme.pdf" onclick="window.open(this.href); return false;" class="normal-a centrer">
                <img src="../annex/images/formation/rgpd-validation" alt="" class="go-returned">
            </a>
            <div class="label-prestation">
                <p>Conformité au RGPD</p>
            </div>
        </div>
    </div>
</div>
<h2 class="centrer ent">Agenda <strong>formation</strong></h2>
<!-- Section du filtre --->
 <?php if (isset($AllfromFormation) AND !empty($AllfromFormation)) : ?>
    <div id="filtre">
        <form method="POST" action="" class="filtre" name="jesaispas">
            <!-- Selecteur de la formation --->
            <div class="conteneur_select">
                <select name="Formation" class="formation">
                    <option class="selected" name="baseFormation" disabled selected>Formation</option>
                    <?php
                    //Foreach qui parcours toute la colonne "type" contenant les formations afin d'assigner chaque ligne, de cette même colonne,à chaque option du select
                    foreach ($Type as $type_formation) {
                    ?>
                        <option class="selected" name="Formation"> <?= $type_formation["type"]; ?> </option>;

                    <?php
                    }
                    ?>
                </select>
                <!-- Selecteur de la ville --->
                <select name="Lieu" class="lieu">
                    <option class="selected" name="baseLieu" disabled selected>Ville</option>
                    <?php
                    //Foreach qui parcours toute la colonne "Ville" contenant les Villes afin d'assigner chaque ligne, de cette même colonne,à chaque option du select
                    foreach ($Ville as $ville_formation) {
                    ?>
                        <option class="selected" name="Lieu"> <?= $ville_formation["ville"]; ?> </option>;
                    <?php
                    }
                    ?>
                </select>
                <input type="button" name="Reset" value="Réinitialiser" class="reset btn-formation input-none" onclick="jesaispas.reset()">
            </div>
        </form>
        <div id="resultat">
            <!-- Section qui affiche le résultat du filtre --->

            <?php
            if ($AllfromFormation != null) {
            ?>
                <table class="events-table poup">
                    <thead>
                        <tr>
                            <th>Intitulé</th>
                            <th>Formation</th>
                            <th>Ville</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <?php
                }
                $i = 0;
                foreach ($AllfromFormation as $base) {
                    if ($i % 2) { ?>
                        <tr class="colorRes">
                        <?php
                    } else {
                        ?>
                        <tr class="contenu-table">
                            <?php
                        }
                            ?>
                            <th id="Nom"><a class="normal-a" href="../fr/registration.php?idform=<?= $base["id_form"]; ?>"><?= $base["nom"]; ?></a></th>
                            <th id="Type"><a class="normal-a" href="../fr/registration.php?idform=<?= $base["id_form"]; ?>"><?= $base["type"]; ?></a></th>
                            <th id="Ville"><a class="normal-a" href="../fr/registration.php?idform=<?= $base["id_form"]; ?>"><?= $base["ville"]; ?></a></th>
                            <th id="Date"><a class="normal-a" href="../fr/registration.php?idform=<?= $base["id_form"]; ?>"><?= date_convert("Le", $base["start_formation"]) ?>
                                
                            </a></th>
                        </tr>
                    <?php
                    $i++;
                }
                $tab_form->closeCursor(); // Termine le traitement de la requête
                    ?>
                </table>
        </div> <!-- Importation obligatoire pour utiliser la syntaxe JQUERY -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- ----------------------------------------------------------------------->
        <script>
            $(".filtre").change(function() {
                $.post(
                    '../fr/filtre.php', {
                        Formation: $(".formation").val(),
                        Lieu: $(".lieu").val(),
                    },
                    function(data) {
                        document.getElementById('resultat').innerHTML = data;
                    },
                    'text'
                );
            });
            $(".reset").click(function() {7
                $(".filtre")[0].reset();
                $.post(
                    '../fr/filtre.php', {
                        Formation: $(".formation").val(),
                        Lieu: $(".lieu").val(),
                    },
                    function(data) {
                        document.getElementById('resultat').innerHTML = data;
                    },
                    'text'
                );
            });
        </script>
    </div>
<div class="section disp">
    <p>Une formation vous intéresse ? Cliquez dessus pour vous inscrire. Pour plus d'informations, <a href="../fr/contact.php" class="normal-a "><strong>contactez moi</strong>.</a></p>
</div>
<?php else :?>
    <div class="section disp">
        <p>Aucune formation n'est disponible pour le moment.</p>
</div>
<?php endif ?>
</body>
</html>

<script src="https://unpkg.com/scrollreveal"></script>
<script type="text/javascript" src="../JS/interaction.js"></script>


<?php
include '../include/footer.inc';
?>
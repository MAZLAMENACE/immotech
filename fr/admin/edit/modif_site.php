<?php
session_start();
include '../../../include/header_admin.inc';
if ($_SESSION['acces'] == 'OK') {
} else {
    include '../../admin/denie_acces.php';
}
require "../../src/help.php";
$bdd = get_pdo();


if(isset($_POST['edit-mail']) AND !empty($_POST['edit-mail']))
{
    if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i", $_POST['edit-mail'])){
    $requete_maj=$bdd->prepare("UPDATE administrateur SET email = :email ");
    $requete_maj->bindValue(':email', $_POST['edit-mail']);
    $requete_maj->execute();
    $requete_maj->closeCursor();
    status("Votre adresse mail de reception a bien été modifiée !");

    }else {
        $message_erreur = 1;
    }
}

if(isset($_POST['edit-mdp']) AND !empty($_POST['edit-mdp'])){
    if (strlen($_POST['edit-mdp']) >= 7 AND preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/',$_POST['edit-mdp'])){
        edit_mdp_secure($_POST['edit-mdp']);
        status("Votre mot de passe a été modifié avec succès");
    }
    else
    {
        $message_erreur = 2;
    }
}

function edit_mdp_secure($mdp): void{
    $bdd = get_pdo();
    $stat = $bdd->prepare('UPDATE administrateur set mdp = :mdp');
    $stat->bindValue(':mdp', md5($mdp));
    $stat->execute();
    $stat->closeCursor();
}


?>

<!DOCTYPE html>
<html>

<head>
    <title>Admin</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/variables_style.css" />
    <link rel="stylesheet" type="text/css" href="../../../CSS/font_style.css" />
    <link rel="stylesheet" type="text/css" href="../../../CSS/button.css" />
    <link rel="stylesheet" type="text/css" href="../../../CSS/admin/modif_style.css" />
</head>

<body>
    <div id="conteneur_modif">
    <div class="menu_vertical">
        <h1 class="centrer t-white">Edition</h1>
    </div>
    <div class="zone_modif">
        <div >
            <form method="POST">
            <label>Adresse mail de reception</label>
            <input type="text" name="edit-mail" class="PartF" placeholder="adresse-reception@gmail.com" <?php if(isset($_POST['edit-mail'])) { echo "value='".$_POST['edit-mail']."'"; } ?>>
            <label>Modifier mon mot de passe</label>
            <input type="password" name="edit-mdp" id="mdp">
        <?php
            $message_erreur = isset($message_erreur) ? $message_erreur : 0;
            if ($message_erreur == 1) {

        ?>
            
            <div>
                <p class="section">Veuillez entrer une adresse mail valide</p>
                <style>.section{ padding: 20px; margin-bottom: 20px; background-color: var(--bad); color:white; }
                </style>
            </div>
        <?php }else if (isset($message_erreur) AND !empty($message_erreur) AND $message_erreur = 2){ ?>
             <div class="section">
                <p>Votre mot de passe n'est pas assez sécurisé.</p><br>
                <p>Pour rappel, votre mot de passe doit au moins avoir :</p><br>
                <ul>
                    <li>7 caractères au minimum</li>
                    <li>Un caractère majuscule</li>
                    <li>Un caractère minuscule</li>
                    <li>Un chiffre</li>
                    <li>Un caractère spécial ( @ # -_ $% ^ & + = § !? )</li>
                </ul>
                <style>.section{ flex-direction: column; padding: 20px; margin-bottom: 20px; background-color: var(--bad); color:white; width: auto;}

                </style>
            </div>
        <?php
        
            $message_erreur = 0 ;
        }
        ?>
        <button class="submit_formulaire" type="submit"  name="editmdpormail">Envoyer</button>
        </form>
        </div>
            <style>form{margin-top: 25px;}</style> 
        </div>        
    
</body>
</html>
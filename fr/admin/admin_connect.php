<?php
session_start();

require "../src/help.php";

//connexion a la pdo
$bdd = get_pdo();

// VERIFICATION CONNEXION

// si les posts existent
if (isset($_POST) AND !empty($_POST)) {
  // on récupère les infos dans les post
  $ndc = htmlentities($_POST['ndcUser']);
  $mdp = htmlentities(md5($_POST['mdpUser']));
  // si ils ne sont pas vides
  if (!empty($ndc) AND !empty($mdp)) {
    // on récupère les données dans la bdd
    $stat = $bdd->prepare('SELECT * FROM administrateur WHERE ndc = ? AND mdp = ?');
    $stat->execute([$ndc, $mdp]);
    $userExist = $stat->rowCount();
    if ($userExist == 1) {
      $userInfos = $stat->fetch();
      $_SESSION['ndc'] = $userInfos['ndc'];
      $_SESSION['mail'] = $userInfos['mail'];
      $_SESSION['acces'] = 'OK';
      header("Location: admin.php");
    }
    // sinon on renvoie une erreur
    else{
      status("Aïe ! Les identifiants sont incorrects.", "error");
      sleep(2);
    }
  }
  // sinon on renvoie une erreur
  else{
    status("Hmm.. On dirait bien que tous les champs ne sont pas remplis...", "error");
  }
}


?>
<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="../../CSS/admin/admin_connect.css" />
  <link rel="stylesheet" type="text/css" href="../../CSS/font_style.css" />
  <link rel="stylesheet" type="text/css" href="../../CSS/variables_style.css" />
  <link rel="stylesheet" type="text/css" href="../../CSS/responsive.css" />
</head>

<body>
  <div id="form_admin">
    <form action="" method="POST">
      <img src="/annex/images/login.svg">
      <h3 class="centrer">Connexion</h3>
      <input type="text" name="ndcUser" placeholder="Nom de compte" >
      <input type="password" name="mdpUser" placeholder="Mot de passe">
      <input type="submit" name="Envoyer" class="submit_formulaire">
    </form>
  </div>
</body>
</html>

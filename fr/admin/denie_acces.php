<?php
// Si l'utilisateur tente d'accéder à la ressource via l'URI
if ("GET" === $_SERVER["REQUEST_METHOD"]) {
    // Renvoie l'utilisateur à la racine du serveur
    header("Location: /");
    // Met fin au script par mesure de sécurité
    die();
}

<?php 
session_start();

require '../../src/help.php';
include '../../../include/header_admin.inc';
include "../../../include/nav_blog.inc";

if ($_SESSION['acces'] == 'OK') {
} else {
    include '../../admin/denie_acces.php' ;
}

$bdd = get_pdo();
$artEdit = $bdd->prepare('SELECT * FROM news WHERE id= :num');
$artEdit->bindValue(':num', $_GET['numArticle'], PDO::PARAM_INT);
$artEditIsOk = $artEdit->execute();
$articles = $artEdit->fetch();


?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../../CSS/variables_style.css" />
    <link rel="stylesheet" type="text/css" href="../../../CSS/font_style.css" />
    <link rel="stylesheet" type="text/css" href="../../../CSS/button.css" />
    <link rel="stylesheet" type="text/css" href="../../../CSS/admin/modif_style.css" />
</head>
<body>
	<script type="text/javascript" src="../../src/ckeditor/ckeditor.js"></script>
	<div class="zone_modif">
		<h2 class="centrer">Modifier <strong>l'article</strong></h2>
		<hr class="hrred">
		<form action="" method="POST" class="form_blog" enctype="multipart/form-data">
			<!-- on cache l'id -> useless pour l'utilisateur -->
			<input type="hidden" name="numArticle" value="<?= $articles['id']; ?>">

			<label>Titre de l'article</label><br>
			<input name="titre" value="<?= $articles['titre']; ?>"></input><br>

			<label>Contenu de l'article</label><br>
			<textarea name="data" class="gl-textarea" id="editor"><?= $articles['contenu']; ?></textarea><br>

			<input type="submit" name="Envoyer !"class="submit_formulaire" value="Enregistrer ?">
		</form>
		<script type="text/javascript">
			CKEDITOR.replace('editor');
		</script>	
	</div>
</body>
</html>

<?php 

if (!empty($_POST) AND isset($_POST)){
	$modif = $bdd->prepare('UPDATE news SET titre=:titre, contenu=:contenu WHERE id =:num LIMIT 1');

$modif->bindValue(':num', $_POST['numArticle']);
$modif->bindValue(':titre', $_POST['titre']);
$modif->bindValue(':contenu', $_POST['data']);



$modifIsOk = $modif->execute();

	if ($modifIsOk) {
		status('Votre article a bien été modifié !');
		
	
	}else{
		echo "Oops";
	}
}



?> 
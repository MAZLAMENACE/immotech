<?php 
session_start();
if ($_SESSION['acces'] == 'OK') {
} else {
	include '../../admin/denie_acces.php';
}

require '../../src/help.php';
require '../../src/Calendar/Events.php';
require '../../src/Calendar/Event.php';
include '../../../include/header_admin.inc';
include "../../../include/nav_blog.inc";

try {
	$bdd = get_pdo();

}catch (Exception $e) {
	echo $e->getMessage();
}


?>
<!DOCTYPE html>
<html>

<head>
	<title>Admin</title>
	<link rel="stylesheet" type="text/css" href="../../../CSS/variables_style.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/font_style.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/button.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/admin/modif_style.css" />
</head>
<body>
	<script type="text/javascript" src="../../src/ckeditor/ckeditor.js"></script>
	<div class="zone_modif">
		<h2 class="centrer ent2">Ajouter un <strong>article</strong></h2>
		<form action="" method="POST" enctype="multipart/form-data">
			<label>Titre de l'article</label><br>
			<input name="titre" <?php if(isset($_POST['titre'])) { echo "value='".$_POST['titre']."'"; } ?>></input><br>
			<label>Contenu de l'article</label><br>
			<textarea name="data" class="gl-textarea" id="editor"><?php if(isset($_POST['data'])) { echo $_POST['data']; } ?></textarea><br>
			<label for="file" class="input-label">Ajouter une photo</label><br>
			<input id="file" type="file" name="imageblog" class="input-file" accept="image/x-png,image/jpeg,image/gif">
			<input type="submit" name="Envoyer !"class="submit_formulaire">
		</form>
		<script type="text/javascript">
			CKEDITOR.replace('editor');
		</script>
		
	</div>
</body>
</html>
<?php
    if(isset($_POST) AND !empty($_POST)){
    	if(isset($_POST["titre"]) AND !empty($_POST["titre"])){
    		if (isset($_FILES['imageblog']['name']) AND !empty($_FILES['imageblog']['name'])) {
		      $id = '\N';
		      $titre = $_POST['titre'];
		      $data = $_POST['data'];
		   	  $image = $_FILES['imageblog']['name'];
		      $nomCheminImageTemporaire = $_FILES['imageblog']['tmp_name'];
		      $nomCheminImageDefinitif = '../upload/'.$_FILES['imageblog']['name'];
		      move_uploaded_file($nomCheminImageTemporaire, $nomCheminImageDefinitif);

		      date_default_timezone_set ('Europe/Paris');
		      $dateajout = date('Y-m-d H:i:s');

		      try {
		        $req = $bdd->prepare('INSERT INTO news VALUES(:id,:titre,:contenu,:dateajout,:image)');
		        $reqIsOk = $req->execute(array(
		          'id' => $id,
		          'titre' => $titre,
		          'contenu' => $data,
		          'dateajout' => $dateajout,
		          'image' => $image
		        ));
				  	if ($reqIsOk){
				  		status("Votre article a bien été ajouté !");
				  		header("Refresh: 5;url=modif_blog.php");
				  	}
		      }catch (Exception $e) {
		        echo($e->getMessage());
		        }

		    }else{
	    		status("N'oubliez pas d'illustrer cet article !", "error");
	    		}
	    }else{
    		status("L'article doit avoir un nom !", "error");
    		}
		

    }
?>

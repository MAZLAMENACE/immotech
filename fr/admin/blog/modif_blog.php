<?php

session_start();
if ($_SESSION['acces'] == 'OK') {
} else {
    include '../../admin/denie_acces.php' ;
}
require '../../src/help.php';
include '../../../include/header_admin.inc';
include "../../../include/nav_blog.inc";


$bdd = get_pdo();
// LA FONCTION CONTIENT TOUS LES CHAMPS DE LA TABLE NEW
$statement = $bdd->prepare('SELECT * FROM news ORDER BY dateajout desc');
$statOk = $statement->execute();
$list_article = $statement->fetchAll();

$statement = $bdd->prepare('SELECT * FROM administrateur');
$stat = $statement->execute();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/variables_style.css" />
    <link rel="stylesheet" type="text/css" href="../../../CSS/font_style.css" />
    <link rel="stylesheet" type="text/css" href="../../../CSS/button.css" />
    <link rel="stylesheet" type="text/css" href="../../../CSS/admin/modif_style.css" />
</head>

<body>
    <div class="zone_modif">
        <h2 class="centrer ent2">Mes <strong>articles</strong></h2>
        <?php if (isset($list_article) AND !empty($list_article)): ?>
        <table class="tab_form">
            <thead>
                <th class="bold">Titre</th>
                <th class="bold">Date</th>
                <th class="bold">Actions</th>
            </thead>
            <?php foreach ($list_article as $article) : ?>
                <tr class="hoverArticle">
                    <td><p><?= $article['titre'] ?></p></td>
                    <td><p><?= date_convert("Crée le",$article['dateajout']) ?></p></td>
                    <td class="centrer">
                        <a class="normal-a" href="delb.php?numArticle=<?= $article['id'] ?>" OnClick="return confirm('Voulez vous vraiment supprimer l\'article <?= $article['titre']?> ?');">
                        <svg class="tailleIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path d="M424 64h-88V48c0-26.5-21.5-48-48-48h-64c-26.5 0-48 21.5-48 48v16H88c-22.1 0-40 17.9-40 40v32c0 8.8 7.2 16 16 16h384c8.8 0 16-7.2 16-16v-32C464 81.9 446.1 64 424 64zM208 48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16v16h-96V48z"/><path d="M78.4 184c-2.9 0-5.1 2.4-5 5.2l13.2 277c1.2 25.6 22.3 45.7 47.9 45.7h243c25.7 0 46.7-20.1 47.9-45.7l13.2-277c0.1-2.9-2.1-5.2-5-5.2H78.4zM320 224c0-8.8 7.2-16 16-16s16 7.2 16 16v208c0 8.8-7.2 16-16 16s-16-7.2-16-16V224zM240 224c0-8.8 7.2-16 16-16s16 7.2 16 16v208c0 8.8-7.2 16-16 16s-16-7.2-16-16V224zM160 224c0-8.8 7.2-16 16-16s16 7.2 16 16v208c0 8.8-7.2 16-16 16s-16-7.2-16-16V224z"/></svg></a>
                        <a class="normal-a " href="modifb.php?numArticle=<?= $article['id'] ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="tailleIcon"><polygon points="51.2 353.3 0 512 158.7 460.8 "/><rect x="89.7" y="169.1" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -95.8583 260.3698)" width="353.3" height="153.6"/><path d="M504.3 79.4L432.6 7.7c-10.2-10.2-25.6-10.2-35.8 0l-23 23 107.5 107.5 23-23C514.6 105 514.6 89.6 504.3 79.4z"/></svg>
                        </a>
            
                    </td>
                    
                </tr>

            <?php endforeach ?>
        </table>
        <?php else : ?>
                <p class="centrer disp">Aucun article n'a été posté pour le moment.</p> 
        <?php endif ?>


    </div>
    </div><!-- div nav -->
</body>

</html>
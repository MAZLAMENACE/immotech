<?php 
session_start();
if ($_SESSION['acces'] == 'OK') {
} else {
	include '../../admin/denie_acces.php';
}


require '../../src/help.php';
require '../../src/Calendar/Events.php';
require '../../src/Calendar/Event.php';
include "../../../include/header_admin.inc";
include "../../../include/nav_agenda.inc";

try {
	$bdd = get_pdo();

}catch (Exception $e) {
	echo $e->getMessage();
}


?>
<!DOCTYPE html>
<html>

<head>
	<title>Admin</title>
	<link rel="stylesheet" type="text/css" href="../../../CSS/variables_style.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/font_style.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/button.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/admin/modif_style.css" />
</head>
	<div class="zone_modif">
		<h2 class="centrer ent2">Ajouter une <strong>formation</strong></h2>
		<form action="" method="POST" class="form_1">
			<label>Nom de la formation</label><br>
			<input type="text" name="nom" class ="form_1_inp"><br>
			<label>Type de formation</label><br>
			<input type="text" name="type">
			<label>Adresse</label>
			<input type="text" name="adresse">
			<label>Ville</label>
			<input type="text" name="ville">
			<label>Code postal</label>
			<input type="number" name="cp" placeholder="ex : 31000"><br>
			<label>Description</label><br>
			<textarea name="description" placeholder="Simple description..."></textarea><br>
			<label>Date de début</label><br>
			<input type="datetime-local" name="start"><br>
			<label>Date de fin</label><br>
			<input type="datetime-local" name="end" ><br>
			<input type="submit" name="Envoyer !"class="submit_formulaire">
		</form>
	</div>
</body>
</html>

<?php
    if(!empty($_POST)){
      $id = '\N';
      $nom = $_POST['nom'];
      $type = $_POST['type'];
	  $adresse = $_POST['adresse'];
	  $ville = $_POST['ville'];
	  $cp = $_POST['cp'];
	  $description = $_POST['description'];
	  $date_s = $_POST['start'];
      $date_e = $_POST['end'];
	  $date_format_s = str_replace("T", " ", $date_s);
      $date_format_e = str_replace("T", " ", $date_e);

      try {
        $req = $bdd->prepare('INSERT INTO formation VALUES(:id,:nom,:type,:adresse,:ville,:cp,:description,:start,:end)');
        $reqIsOk = $req->execute(array(
          'id' => $id,
          'nom' => $nom,
          'type' => $type,
          'adresse'=> $adresse,
          'ville' => $ville,
          'cp' => $cp,
          'description' => $description,
          'start' => $date_format_s,
          'end' => $date_format_e
          
        ));
	  	if ($reqIsOk){
	  		status("Votre formation a bien été enregistrée");
	  		header("Refresh: 5;url=calendrier.php");
	  	}else{
	  		echo "Oh non ! Votre formation n'a pas été enregistrée ";
	  	}

      } catch (Exception $e) {
        echo($e->getMessage());
      }
    };
?>

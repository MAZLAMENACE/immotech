<?php 
session_start();
if ($_SESSION['acces'] == 'OK') {
} else {
	include '../../admin/denie_acces.php';
}


?>


<!DOCTYPE html>
<html>
<head>
	<title>Calendrier</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="/CSS/admin/calendrier.css">
</head>
	<body>
		<!-- <nav class="navbar navbar-dark bg-primary nb-3">
			<a href="/fr/calendrier.php" class="navbar-brand">Le calendrier</a>
		</nav> -->
	<?php
	include "../../../include/header_admin.inc";
	// récupération de la class Mois
	require '../../src/help.php';
	require '../../src/Calendar/Mois.php';
	require '../../src/Calendar/Events.php';

	$pdo = get_pdo();
	$events = new Calendar\Events($pdo);
	$month = new Calendar\Mois($_GET['month'] ?? null, $_GET['year'] ?? null);
	$start = $month->getFirstDay();
	$start = $start->format('N') === '1' ? $start : $month->getFirstDay()->modify('last monday');
	$weeks = $month->getWeeks();
	$end = (clone $start)->modify('+'. (6 + 7 * ($weeks - 1)) . 'days');
	$events = $events->getEventsBetweenByDay($start, $end);
	// var_dump($events);
	  ?>
	  <div class="d-flex flex-row align-items-center justify-content-between mx-sm-3">
	  	<h1 class="couleur_primaire no_p"><?= $month->toString();?></h1>
		  <div>
		  	<a href="/fr/admin/agenda/calendrier.php?month=<?= $month->previousMonth()->month;?>&year=<?= $month->previousMonth()->year;?>" class="btn btn-danger">&lt;</a>
		  	<a href="/fr/admin/agenda/calendrier.php?month=<?= $month->nextMonth()->month;?>&year=<?= $month->nextMonth()->year;?>" class="btn btn-danger">&gt;</a>
		  </div>
	  </div>
	  <table class="calendrier">
	  	<?php for ($i=0; $i < $weeks ; $i++): ?>
	  		<tr>
				<?php foreach ($month->days as $k => $day):
					$date = (clone $start)->modify("+". ($k + $i *7)."days");
					$eventsDay = $events[$date->format('Y-m-d')] ?? [];
				 ?>
				<td class="<?= $month->dansLeMois($date) ? '' : 'dejadanslemois'?>">
					<?php if ( $i === 0):?>
						<div class="stringDay"><?= $day;?></div>
					<?php endif ?>		
					<div class = "intDay"><?= $date ->format('d'); ?></div>
					<?php foreach($eventsDay as $event): ?> 
					<div class="day_event">
					<?= (new DateTime($event['start_formation']))->format('H:i')?> - <a href="event.php?id=<?= $event['id_form'];?>"><?= $event["nom"];?></a>
					</div>
					<?php endforeach; ?> 
				</td>
				<?php endforeach;?>
	  		</tr>
	  	<?php endfor ;?>
	  </table>
<a href="/fr/admin/agenda/add.php" class="button_calendrier">+</a>
	</body>
</html>
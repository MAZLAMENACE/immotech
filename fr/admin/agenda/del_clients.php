<?php 
require '../../src/help.php';
$bdd = get_pdo();
// Requete pour supprimer un client
$delclient = $bdd->prepare('DELETE FROM client WHERE id=:num LIMIT 1');
$delclient->bindValue(':num', $_GET['numClient'], PDO::PARAM_INT);
$delIsOK = $delclient->execute();


if ($delIsOK){
	status("Le client a bien été supprimé ! !");
	header("Location: modif_agenda.php");
    exit();
}else{
	echo "Echec de la suppression !";
}

?>
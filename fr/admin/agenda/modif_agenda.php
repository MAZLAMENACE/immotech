<?php

session_start();
if ($_SESSION['acces'] == 'OK') {
} else {
	include '../../admin/denie_acces.php';
}

include '../../../include/header_admin.inc';
include "../../../include/nav_agenda.inc";
include "../../src/help.php";

// récupration de la bdd
$bdd = get_pdo();
// LA REQUETE CONTIENT TOUS LES CHAMPS DE LA TABLE FORMATION
$statement = $bdd->prepare('SELECT * FROM client');
// éxécution de la requete
$statOk = $statement->execute();
// récupration du tableau associatif
$list_client = $statement->fetchAll();

/*SELECTION DE LA FORMATION DU CLIENT*/
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Admin</title>
		<link rel="stylesheet" type="text/css" href="../../../CSS/variables_style.css" />
		<link rel="stylesheet" type="text/css" href="../../../CSS/font_style.css" />
		<link rel="stylesheet" type="text/css" href="../../../CSS/admin/modif_style.css" />
	</head>
	<body>
		<div class="zone_modif">
			<h2 class="centrer ent2">Liste de mes <strong>clients</strong></h2>
			<?php if (isset($list_client) AND !empty($list_client)):?>
			<table class="tab_client">
				<thead>
					<th class="bold">Nom</th>
					<th class="bold">Prénom</th>
					<th class="bold">Email</th>
					<th class="bold">Inscrit à</th>
					<th class="bold">Actions</th>
				</thead>
			<?php foreach ($list_client as $client): ?>
				<tr class="hoverArticle">
					<td><p><?= $client['nom'] ?></p></td>
					<td><p><?= $client['prenom'] ?></p></td>
					<td><p><?= $client['email'] ?></p></td>
				<?php $idforma = $client['idformation'];
                $stat = $bdd->prepare("SELECT * FROM formation WHERE id_form = $idforma ");
                $statOk = $stat->execute(); ?>
				<?php foreach ($stat as $listforma): ?>
					<td><p><?= $listforma['nom'] ?></p></td>
				<?php endforeach ?>
				<td>
					<a class="normal-a" href="del_clients.php?numClient=<?= $client['id'];?>" OnClick="return confirm('Voulez vous vraiment supprimer <?= $client['nom']?> ?');">
                        <svg class="tailleIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path d="M424 64h-88V48c0-26.5-21.5-48-48-48h-64c-26.5 0-48 21.5-48 48v16H88c-22.1 0-40 17.9-40 40v32c0 8.8 7.2 16 16 16h384c8.8 0 16-7.2 16-16v-32C464 81.9 446.1 64 424 64zM208 48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16v16h-96V48z"/><path d="M78.4 184c-2.9 0-5.1 2.4-5 5.2l13.2 277c1.2 25.6 22.3 45.7 47.9 45.7h243c25.7 0 46.7-20.1 47.9-45.7l13.2-277c0.1-2.9-2.1-5.2-5-5.2H78.4zM320 224c0-8.8 7.2-16 16-16s16 7.2 16 16v208c0 8.8-7.2 16-16 16s-16-7.2-16-16V224zM240 224c0-8.8 7.2-16 16-16s16 7.2 16 16v208c0 8.8-7.2 16-16 16s-16-7.2-16-16V224zM160 224c0-8.8 7.2-16 16-16s16 7.2 16 16v208c0 8.8-7.2 16-16 16s-16-7.2-16-16V224z"/></svg>
                    </a>
				</td>
				</tr>
			<?php endforeach ?>
			</table>
			<?php else : ?>
				<p class="centrer disp">Vous n'avez pas encore de clients inscrits.</p>
			<?php endif?>
     
		</div>
	</body>
</html>
<?php 


require '../../src/help.php';
$bdd = get_pdo();
// Requete pour supprimer l'article
$del = $bdd->prepare('DELETE FROM formation WHERE id_form=:num LIMIT 1');
$del->bindValue(':num', $_GET['numFormation'], PDO::PARAM_INT);
$delIsOK = $del->execute();


if ($delIsOK){
		status("Votre formation a bien été supprimée !");
	  	header("Location: ../agenda/calendrier.php");
    exit();
}else{
	echo "Echec de la suppression !";
}



?> 
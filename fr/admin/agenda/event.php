<?php 
session_start();
if ($_SESSION['acces'] == 'OK') {
} else {
	include '../../admin/denie_acces.php';
}
require_once "../../src/help.php";
require "../../src/Calendar/Events.php";

$pdo = get_pdo();
$events = new Calendar\Events($pdo);


 if (!isset($_GET['id'])){
 	e404();
 }
 
 try {
 	$event = $events->find($_GET['id']);

 } catch (\Exception $e) {
 	e404();
 }
include "../../../include/header_admin.inc";

// affichage des clients
$affclient = $pdo->prepare('SELECT * FROM client WHERE idformation = :idforma');
$affclient->bindValue(':idforma', $event['id_form'], PDO::PARAM_INT);
$affclient->execute();
$clients = $affclient->fetchAll();


?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../../../CSS/variables_style.css" />
		<link rel="stylesheet" type="text/css" href="../../../CSS/font_style.css" />
		<link rel="stylesheet" type="text/css" href="../../../CSS/admin/modif_style.css" />
	</head>
	<style type="text/css">
		.container-event{
			display: flex;
			flex-direction: row;
			width: 100%;
		}
		.left{width: 40%;}
		.right{ width: 60%; }
		.container-event ul{padding-left: 50px;}
		h3{margin-top:20px;}
	</style>
	<body>
	<div class="container-event">
		<div class="left">
		<h3 class="decaleg centrer"><?= htmlentities($event['nom']) ?></h3>
		<br>
		<ul>
			<li><em>Type :</em> <?= htmlentities($event['type']); ?></li>
			<li><em>Date :</em> <?= (new DateTime($event['start_formation']))->format('d/m/Y');?> </li>
			<li><em>Heure de démarrage :</em> <?= (new DateTime($event['start_formation']))->format('H:i');?></li>
			<li><em>Heure de fin :</em> <?= (new DateTime($event['end_formation']))->format('H:i');?></li>
			<li><em>Adresse :</em> <?= htmlentities($event['adresse']); ?> </li>
			<li><em>Description</em><br>
				<?= htmlentities($event['description']) ;?> 
			</li>
			<br>
			<a href="del.php?numFormation=<?= $event['id_form']?>"OnClick="return confirm('Voulez vous vraiment supprimer la formation <?= $event['nom'] ?> ?');">
				<svg class="tailleIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
		        <path d="M424 64h-88V48c0-26.5-21.5-48-48-48h-64c-26.5 0-48 21.5-48 48v16H88c-22.1 0-40 17.9-40 40v32c0 8.8 7.2 16 16 16h384c8.8 0 16-7.2 16-16v-32C464 81.9 446.1 64 424 64zM208 48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16v16h-96V48z"/><path d="M78.4 184c-2.9 0-5.1 2.4-5 5.2l13.2 277c1.2 25.6 22.3 45.7 47.9 45.7h243c25.7 0 46.7-20.1 47.9-45.7l13.2-277c0.1-2.9-2.1-5.2-5-5.2H78.4zM320 224c0-8.8 7.2-16 16-16s16 7.2 16 16v208c0 8.8-7.2 16-16 16s-16-7.2-16-16V224zM240 224c0-8.8 7.2-16 16-16s16 7.2 16 16v208c0 8.8-7.2 16-16 16s-16-7.2-16-16V224zM160 224c0-8.8 7.2-16 16-16s16 7.2 16 16v208c0 8.8-7.2 16-16 16s-16-7.2-16-16V224z"/></svg>
			</a>				
		</ul>
		</div>
		<div class="right">
			<h3 class="decaleg centrer">Liste des clients pour cette formation</h3>
			<?php if(empty($clients)):?>
			<p class="centrer">Aucun client n'est inscrit à cette formation !</p class="centrer">
			<?php elseif(!empty($clients)): ?>
				<table class="tab_client">
				<thead>
					<th class="bold">Nom</th>
					<th class="bold">Prénom</th>
					<th class="bold">Email</th>
				</thead>
				<?php foreach ($clients as $client): ?>
				<tr>
					<td><p><?= $client['nom']?></p></td>
					<td><p><?= $client['prenom']?></p></td>
					<td><p><?= $client['email']?></p></td>
				</tr>
				<?php endforeach ?>
			</table>
			<?php endif ?>
				
		</div>
	</div>
	</body>
</html>
<?php
session_start();
if ($_SESSION['acces'] == 'OK') {
} else {
    include '../admin/denie_acces.php';
}

include "../../include/header_admin.inc";

?>
<!DOCTYPE html>
<html>

<head>
    <title>Admin</title>
    <link rel="stylesheet" type="text/css" href="../../CSS/admin/admin.css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/variables_style.css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/font_style.css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/button.css" />
</head>

<body>
    
    <div id="title_admin">
        <h2>Bonjour, <strong><?= $_SESSION['ndc']?></strong> !</h2>
        <h4>Bienvenue sur votre espace d'administration. </h4><br>
        <h4>N'oubliez pas de vous <red>déconnecter</red> avant de partir !</h4>
       
    </div>
    <div id="box_admin">
        
    </div>
</body>
<!--   <footer>
        <a href="accueil.php" class="submit_formulaire"> Retour</a>
    </footer> -->

</html>
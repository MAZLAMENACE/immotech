<?php 
include '../include/header.inc';

?>
<!DOCTYPE html>
<html>
<head>
	<title>RGPD</title>
	<link rel="stylesheet" type="text/css" href="../CSS/font_style.css">
	<link rel="stylesheet" type="text/css" href="../CSS/responsive.css">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
	<h2 class="centrer">CHARTE DE PROTECTION DES <strong>DONNEES PERSONNELLES</strong></h2>
		<p class="disp">Respectueuse de la vie privée, LA DEPECHE DU SYNDIC accorde une grande importance à la protection de vos données à caractère personnel. La présente charte vise ainsi à vous exposer en toute transparence les modalités et caractéristiques des traitements de données mis en œuvre par LA DEPECHE DU SYNDIC.</p>
	<h3 class="decaleg">1. QUEL EST NOTRE RESPONSABLE DU TRAITEMENT ?</h3>
		<p class="decaleg">Il s’agit de Mme Viviane LABBE, gérante de la société. <br>
		Voici ses coordonnées :</p>
		<div class="decaleg">
			<p>
				<em>Adresse postale :</em> LA DEPECHE DU SYNDIC – 1015 avenue du Lauragais – 31860 LABARTHE SUR LEZE <br>
				<em>Courriel :</em><a> viviane.labbe@ladepechedusyndic.fr</a><br>
				<em>Téléphone :</em> 06 12 81 93 99 <br>
			</p><br>
		</div>
	<h3 class="decaleg">2. QUELLES SONT LES FINALITÉS ET LA BASE JURIDIQUE DU TRAITEMENT ?</h3>
		<p class="decaleg">
			Les données à caractère personnel collectées par LA DEPECHE DU SYNDIC sont destinées à la réalisation d’opérations de formation ou de conseil :</p>
			<div class="disp">
				<p>– Prospection commerciale (prise de contact, recherche de nouveaux clients ou formateurs) ; <br>
				– Exécution de la prestation (édition des conventions ou contrats, animations, évaluations, communications et tout autre acte y afférent) ; <br>
				– Réponse aux obligations légales, règlementaires, comptables et administratives en vigueur ; <br>
				</p>
			<p><br>
			Le traitement de ces données est obligatoire pour l’accomplissement de ces finalités. A défaut, LA DEPECHE DU SYNDIC pourrait se trouver dans l’incapacité d’exécuter son activité et, notamment, tout ou partie des missions qui lui sont confiées.<br>
			LA DEPECHE DU SYNDIC collecte uniquement les données nécessaires à la finalité du traitement envisagé et ne communique que celles qui sont nécessaires aux prestataires et tiers concernés par l’opération. <br>
			Si LA DEPECHE DU SYNDIC prévoyait d’utiliser vos données personnelles à d’autres fins que celles mentionnées ci-dessus, elle s’engage à vous en informer en premier. Vous aurez alors la possibilité de ne pas donner votre consentement ou de retirer celui-ci. Le traitement s’appuie sur le consentement des personnes concernées, le respect d’une obligation prévue par un texte ou l’exécution d’un contrat.
			</p>
			</div>
	<h3 class="decaleg">3. QUELLES SONT LES CATÉGORIES DE DONNÉES PERSONNELLES CONCERNÉE ?</h3>
	<em class="decaleg">DONNÉES PERSONNELLES FOURNIES PAR VOUS<em>
	<div class="decaleg">
	<ul class="disp">
		<li>Des données relatives à votre identité et vos coordonnées telles que, notamment, vos noms et prénoms, vos adresses postale et électronique, vos numéros de téléphone.</li>
		<li>Des informations relatives à votre situation professionnelle : activité, besoins en formation ou en conseil, formations déjà effectuées, réseaux sociaux ; pour les salariés: employeur, statut, fonction ; pour les entrepreneurs : numéros  d’immatriculation et d’habilitation ou assimilé et s’il y a lieu, organisation, logiciels métier.</li>
		<li>Des données relatives au suivi de votre relation avec LA DEPECHE DU SYNDIC : demandes de contact, de renseignements ou de documentation, correspondances échangées.</li>
		<li>Des données de paiement nécessaires aux virements et prélèvements lorsque c’est nécessaire.</li>
		<li>Et plus généralement, toutes données utiles à l’exercice de l’activité d’organisme de formation ou de coaching.</li>
	</ul>
	</div>
		<em class="decaleg">DONNÉES PERSONNELLES COLLECTÉES AUPRÈS DE TIERS</em>
		<div class="decaleg"> 
		<ul class="disp">
		<li>Des données relatives à votre identité et vos coordonnées nom, prénom, adresses postale et email, téléphone (fixe et/ou mobile) issues de sources accessibles au public (réseaux sociaux, internet, journaux, …) ou non (réseau).</li>
		</ul>
		</div>
	<h3 class="decaleg">4. QUELS SONT LES DESTINATAIRES DES DONNÉES PERSONNELLES COLLECTÉES ?</h3>
	<div class="decaleg">
		<em>DONNÉES COMMUNIQUÉES EN INTERNE :</em><p>néant.</p><br>
		<em>DONNÉES COMMUNIQUÉES A NOS PRESTATAIRES :</em><br>
	</div>
 <p class="disp">LA DEPECHE DU SYNDIC fait appel à d’autres professionnels pour exécuter tel ou tel service dans le cadre des opérations de formation (par exemple, informaticien, formateur). LA DEPECHE DU SYNDIC ne partage que les informations nécessaires afin de permettre à ces derniers de réaliser leurs missions. Ces prestataires sont tenus de respecter la Charte sur la protection des données personnelles de LA DEPECHE DU SYNDIC et de ne pas utiliser vos données personnelles afin de servir leurs propres intérêts commerciaux.</p>
 <em class="decaleg">DONNÉES COMMUNIQUÉES A DES TIERS :</em>
 <p class="disp">LA DEPECHE DU SYNDIC ne partage vos données personnelles avec des tiers qu’avec votre consentement. LA DEPECHE DU SYNDIC est néanmoins susceptible de partager vos données personnelles :</p>
 <ul class="decaleg">
	<li>Dans le cadre d’un contrôle par un organisme privé ou public dont son activité relève (par exemple, DIRECCTE, organisme de certification) ;</li>
	<li>Pour répondre aux assignations, aux injonctions d’un tribunal, aux procédures judiciaires ou à toute autre requête adressée par les autorités compétentes auxquelles nous devons nous soumettre ou pour établir ou exercer des droits,</li>
	<li>Ou encore pour nous défendre contre des actions en justice.
	LA DEPECHE DU SYNDIC se réserve également le droit de transférer vos données personnelles dans le cas où elle vendrait ou transférerait tout ou partie de ses activités ou actifs afin de permettre à l’acheteur de continuer à offrir ses services.</li>
</ul><br>
<em class="decaleg">« PLUG-INS » ET MODULES SOCIAUX</em>
<div class="disp">
	<p>
	Le site internet de LA DEPECHE DU SYNDIC utilise des « plug-in » ou modules sociaux. Il s’agit notamment des boutons « j’aime », « partager » des réseaux sociaux tiers tels que Facebook, Twitter, Linkedin, Viadeo. Ils vous permettent d’aimer (« liker ») et de partager des informations provenant de ce site avec vos amis sur les réseaux sociaux.<br>
	Lorsque vous consultez une page de ce site (web ou mobile) contenant des plugs-in ou modules sociaux, une connexion est établie avec les serveurs des réseaux sociaux qui sont alors informés que vous avez accédé à la page correspondante du site consulté, et ce même si vous ne possédez pas de compte utilisateur Facebook, Twitter, Linkedin, Viadeo et même si vous n’êtes pas connecté à votre compte Facebook, Twitter, Linkedin, Viadeo. <br>
	Si vous ne souhaitez pas que les réseaux sociaux publient vos actions issues des plug-ins dans vos comptes sur les réseaux sociaux, vous devez vous déconnecter de vos réseaux sociaux avant de visiter le site. LA DEPECHE DU SYNDIC vous invite à consulter les politiques de protection de la vie privée de ces réseaux sociaux.
	</p>
</div>
	<h3 class="decaleg">5. DES TRANSFERTS DE DONNÉES PERSONNELLES A DESTINATION D’UN ÉTAT N’APPARTENANT PAS A L’UE SONT-ILS ENVISAGES ?</h3>
	<p class="decaleg">Vos Données Personnelles sont stockées dans les bases de données situées en France ainsi que dans celles de nos prestataires de services.
	Si, pour des raisons essentiellement techniques, certaines données étaient transférées en dehors du territoire de l’Union européenne, LA DEPECHE DU SYNDIC s’engage à vérifier que ses prestataires s’appuient sur la règlementation de l’Union européenne.</p> <br>

	<h3 class="decaleg">6. COMBIEN DE TEMPS ALLONS-NOUS CONSERVER VOS DONNÉES ?</h3>
	<em class="decaleg">DONNÉES AUX FINS DE PROSPECTION COMMERCIALE :</em><br>
	<div class="disp">
		<p>
		Sauf indication contraire de votre part, vos données personnelles sont conservées pour une durée maximale de 3 (trois) ans à compter de la fin de la relation commerciale. Au terme de ces 3 (trois) ans, nous nous engageons à reprendre contact avec vous afin de déterminer si vous souhaitez continuer ou non à recevoir des sollicitations commerciales. Au-delà de cette durée maximale, les données seront archivées et anonymisées ou détruites.<br>
		</p>
	</div>
	<em class="decaleg">AUTRES DONNÉES :</em><br>
	<div class="disp">
	<p>
	Afin de satisfaire à la règlementation notamment en matière de prescription commerciale, vos données sont conservées pour une durée de 10 (dix) ans à compter de la fin de la relation contractuelle en l’absence de causes de suspension ou d’interruption de la prescription.
	L’ensemble de ces données peut toutefois être conservé plus longtemps que les durées précitées après obtention de votre accord, sous forme d’archives, pour répondre aux obligations légales et réglementaires éventuelles qui s’imposent ou encore pendant d’autres délais légaux de prescription applicables.</p><br>
	<p>Les durées de conservation précitées ont été définies de manière à nous permettre de traiter vos demandes (renseignement, contact, service…) et/ou de mener à bien nos opérations commerciales, de communication, de marketing, tout en respectant le principe de proportionnalité selon lequel les données à caractère personnel ne doivent pas être conservées plus longtemps que la durée nécessaire à la réalisation de la finalité pour laquelle elles ont été collectées.</p><br>
	<p>L’archivage de vos données personnelles est effectué sur un support fiable conformément aux législations en vigueur. En cas de litige entre les données ainsi conservées et tout document fourni par vous, il est expressément convenu au titre de convention de preuve que les données recueillies par les soins de LA DEPECHE DU SYNDIC primeront et seront seules admises à titre de preuve.
	</p>
</div>
	<h3 class="decaleg">7. VOS DROITS</h3>
	<em class="decaleg">DROITS D’ACCÈS, DE MODIFICATION, DE LIMITATION, DE SUPPRESSION ET DE PORTABILITÉ DES DONNES PERSONNELLES</em>
	<div class="disp">
		<p>Vous disposez d’un droit général d’accès, de rectification, de limitation et de suppression, droit de ne pas faire l’objet d’une décision individuelle automatisée (y compris profilage) ainsi que d’un droit à la portabilité de l’ensemble des données personnelles vous concernant, collectées au fur et à mesure de votre utilisation de nos services.<br>
		Dans le cas où le traitement repose sur votre consentement, vous avez le droit de le retirer à tout moment et les opérations effectuées avant ce retrait restent licites.<br>
		Vous pouvez à tout moment exercer ces droits en écrivant par courriel à : viviane.labbe@ladepechedusyndic.fr ou courrier postal à l’adresse suivante :
		LA DEPECHE DU SYNDIC – 1015 avenue du Lauragais – 31860 LABARTHE SUR LEZE, en joignant une copie de votre carte d’identité.
		</p>
	</div>
	<em class="decaleg">DROIT D’OPPOSITION</em>
	<div class="disp">
		<p>
		Vous pouvez à tout moment vous opposer au traitement de vos données personnelles ou retirer votre consentement en écrivant par courriel à : <em>viviane.labbe@ladepechdusyndic.fr</em></p><br>
		<p>
	ou courrier postal à l’adresse suivante : LA DEPECHE DU SYNDIC – 1015 avenue du Lauragais – 31860 LABARTHE SUR LEZE, en joignant une copie de votre carte d’identité. <br>
	Si vous avez donné votre accord pour recevoir des lettres d’actualités et d’autres messages promotionnels par email, courrier, réseau social et SMS, vous pouvez à tout moment vous opposer à la réception de tout message promotionnel en utilisant le lien de désabonnement fourni ou en adressant un email à viviane.labbe@ladepechedusyndic.fr.<br>
	Le formulaire en ligne sur notre site propose aux utilisateurs de saisir leur numéro de téléphone. L’utilisateur qui ne souhaite pas faire l’objet de prospection notamment commerciale par voie téléphonique est informé de son droit à s’inscrire gratuitement sur la liste nationale d’opposition au démarchage téléphonique via le site internet accessible à l’url <a href ="http://www.bloctel.gouv.fr">www.bloctel.gouv.fr</a>
		</p>
	</div>
	<em class="decaleg">DROIT D’INTRODUIRE UNE RÉCLAMATION</em>
	<div class="disp">
	<p>
	Vous disposez en tout état de cause de la possibilité d’introduire une réclamation auprès d’une autorité nationale en charge de la protection des données à caractère personnel (en France, il s’agit de la Commission Nationale de l’Informatique et des Libertés ou « Cnil ») si vous estimez que le traitement de vos données n’est pas effectué conformément aux dispositions applicables.
	</p>
	</div>
	<h3 class="decaleg">8. MISE A JOUR DE LA CHARTE</h3>
	<div class="disp">
		<p>
		Nous nous réservons le droit de modifier la présente Charte, à tout moment, en tout ou partie compte tenu des modifications et évolutions de nos usages et procédures internes.
Nous vous invitons à consulter régulièrement cette Charte, avant toute collecte de vos données.
		</p>
	</div>
</body>
</html>

<?php 
include '../include/footer.inc';

?>
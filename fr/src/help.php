<?php
function e404()
{
	require '404.php';
	exit();
}

// function de débugage
function dd(...$vars)
{
	foreach ($vars as $var) {
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
}

function date_convert($msg = "", $date) : void{
	$datef = new DateTime($date);
	$tabMois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
	// getter pour le format
	$getNbMois = $datef->format('n');
	$getNbJour = $datef->format('j');
	$getNbAnnee = $datef->format('Y');
	$getHeure = $datef->format('H');
	$getMin = $datef->format('i');
	$formatMois = $tabMois[$getNbMois-1];
	echo $msg." ".$getNbJour." ". $formatMois." ".$getNbAnnee." à ".$getHeure."h".$getMin;
}

// fonction pour se connecter à la pdo - configurée de base pour localhost
function get_pdo($dbtype = 'mysql', $host = 'localhost', $port = '3308', $dbname = 'agenda', $user = 'root', $mdp = ''): \PDO
{
	return new \PDO("$dbtype:host=$host:$port;dbname=$dbname", "$user", "$mdp", [
		\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
		\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
	]);
}


function goToIndex(){
	header('location: /');
}

function encrypt_decrypt($pwd)
{
	$bdd = get_pdo();
	$mdpDecrypt = $pwd;
	$encryption_key_256bit = base64_encode(openssl_random_pseudo_bytes(32));
	$encrypted_chaine = openssl_encrypt($pwd, "AES-128-ECB",   $encryption_key_256bit);
	$decrypted_chaine = openssl_decrypt($encrypted_chaine, "AES-128-ECB", $encryption_key_256bit);
	if ($pwd != $encrypted_chaine) {
		//encrypt le mdp du compte admin actif
		$pwd = $encrypted_chaine;
	} else {
		//decrypt le mdp du dernier compte admin actif
		$pwd = $decrypted_chaine;
	}
	$requete_update = $bdd->prepare("UPDATE administrateur SET mdp = '$pwd' where mdp = '$mdpDecrypt'");
	$requete_update->execute();
}


//fonction qui permet d'envoyer un message (1er argument) et retourner son type (2ème argument) -> la classe est générée dynamiquement par style.css , message erreur = error
function status($msg, $type = "okToSend"){
	echo "<div class =".$type."><p>".$msg."</p></div>";
}


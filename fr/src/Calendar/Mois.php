<?php 
namespace Calendar;

class Mois
{
	public $days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi","Dimanche"];

	private $months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];

	public $month;
	public $year; 

	// $mois compris entre 1 et 12
	public function __construct(?int $month = null, ?int $year = null)
	{
		if ($month === null || $month < 1 || $month > 12){
			$month = intval(date('m'));
		}
		if ($year === null){
			$year = intval(date('Y'));
		}
		
		$this->month = $month;
		$this->year = $year;
	}

	// renvoie le premier jour du mois
	public function getFirstDay():\DateTime{
		return new \DateTime("{$this->year}-{$this->month} -01");
	}

	// retourne le mois en chaine de caractère
	public function toString() : string {
		return $this->months[$this->month - 1]." ".$this->year;
	} 

	// Pour récupérer le nombre de semaine 
	public function getWeeks() : int {
		$start = $this->getFirstDay();
		$end = (clone $start)->modify('+1 month -1 day');
		$weeks = intval($end->format('W')) - intval($start->format('W')) +1;
		if ($weeks < 0 ){
			$weeks = intval($end->format('W'));
		} 
		return $weeks;
	}

	// check si les jours du mois précédent ce chevauchent avec celui en cours
	public function dansLeMois(\DateTime $date): bool{
		return $this->getFirstDay()->format('Y-m') === $date->format('Y-m');
	
	}

	// renvoie le mois suivant
	public function nextMonth() : Mois
	{
		$month = $this->month + 1;
		$year = $this->year;
		if ($month > 12) {
			$month = 1;
			$year += 1;
		}
		return new Mois($month, $year);
	}

	// renvoie le mois précédent
	public function previousMonth() : Mois
	{
		$month = $this->month - 1;
		$year = $this->year;
		if ($month < 1) {
			$month = 12;
			$year -= 1;
		}
		return new Mois($month, $year);
	}
}

?>
<?php 

namespace Calendar;

class Events {

	private $pdo;
	public function __construct(\PDO $pdo)
	{
		$this->pdo = $pdo;

	}

	public function getEventsBetween(\DateTime $start, \DateTime $end ): array{
	$sql = "SELECT * FROM formation WHERE start_formation BETWEEN '{$start->format('Y-m-d 00:00:00')}' AND '{$end->format('Y-m-d 23:59:59')}'";

	$statement = $this->pdo->query($sql);
	$results = $statement->fetchAll();
	return $results;
	}


	public function getEventsBetweenByDay(\DateTime $start, \DateTime $end ): array{
		$events = $this->getEventsBetween($start, $end);
		$days = [];
		foreach ($events as $event) {
			$date = explode( ' ', $event['start_formation'])[0];

			if (!isset($days[$date])) {
				$days[$date] = [$event];

			}else{
				$days[$date][] = $event;
			}
		}
		return $days;
	}

	public function find (int $id) : array{
		$result = $this->pdo->query("SELECT * FROM formation WHERE id_form = $id LIMIT 1")->fetch();
		
		if ($result === false){
			throw new \Exception("Aucun résultat n'a été trouvé !", 1);
			
		}
		return $result;
	}

	public function create(Event $event): bool{
		$statement = $this->pdo->prepare('INSERT INTO events(nom, description, start_formation, end_formation)VALUES(?, ?, ?, ?) ');
		$statement->execute([
			$event->getName(),
			$event->getDescription(),
			$event->getStart()->format('Y-m-d H:i:s'),
			$event->getEnd()->format('Y-m-d H:i:s')		]);


	}

}

?> 
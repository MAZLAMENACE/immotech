<?php 

namespace Calendar;

class Event
{
	private $id;
	private $name;
	private $type;
	private $adresse;
	private $ville;
	private $cp;
	private $description;
	private $start;
	private $end;

	public function getId():int{
		return $this->$id;
	}
	public function getName(){
		return $this->$name;
	}

	public function getType():string{
		return $this->$type;
	}
	public function getAdresse():string{
		return $this->$adresse;
	}
	public function getVille():string{
		return $this->$ville;
	}

	public function getCP():int{
		return $this->$cp;
	}
	public function getDescription():string{
		return $this->$description;
	}
	public function getStart():\DateTime{
		return new \DateTime($this->$start);
	}
	public function getEnd():\DateTime{
		return new \DateTime($this->$end);
	}

	public function SetName(string $name){
		$this->name = $name;
	}
	public function SetDescription(string $description){
		$this->description = $description;
	}
	public function SetStart(string $start){
		$this->start = $start;
	}
	public function SetEnd(string $end){
		$this->end = $end;
	}


}

?>
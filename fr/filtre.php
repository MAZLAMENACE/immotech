<?php

require "./src/help.php";
$bdd = get_pdo();
$queryTypeLieu =
    'SELECT * FROM formation';
if (!empty($_POST["Formation"]) && !empty($_POST["Lieu"])) { //Si on filtre avec les types et les villes                                                                                                                                                                                                                                                                  //si il fonctionne il doit afficher "F & L";
    $queryTypeLieu .= ' WHERE type = :type and ville = :ville AND now() < start_formation order by id_form DESC';
    $tab_form = $bdd->prepare($queryTypeLieu);
    $tab_form->bindValue(':ville', $_POST["Lieu"]);
    $tab_form->bindValue(':type', $_POST["Formation"]);
    $tab_form->execute();
    $TypeLieu = $tab_form->fetchAll(PDO::FETCH_ASSOC);
    if ($TypeLieu == null) { //Si les selections ne correspondent pas dans la bdd 
?>
        <p>
            Aucune correspondance...
        </p>
    <?php
    } else {
    ?>
        <table class="events-table">
            <thead>
                <tr>
                    <th class="event-intitule">Intitulé</th>
                    <th class="event-type">Formation</th>
                    <th class="event-ville">Ville</th>
                    <th class="event-date">Date</th>
                </tr>
            </thead>
            <?php
        }
        $i = 0;
        foreach ($TypeLieu as $base => $val) {
            if ($i % 2) { ?>
                <tr class="colorRes">
                <?php
            } else {
                ?>
                <tr class="">
                <?php
            }
                ?>
                 <th id="Nom"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["nom"]; ?></a></th>
                        <th id="Type"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["type"]; ?></a></th>
                        <th id="Ville"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["ville"]; ?></a></th>
                        <th id="Date"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= date_convert("Le", $val["start_formation"]); ?></a></th>
                </tr>
            <?php
            $i++;
        }
            ?>
        </table>
    <?php
} elseif (!empty($_POST["Lieu"]) && empty($_POST["Formation"])) { //Si on filtre seulement les villes
    $queryTypeLieu .= ' WHERE ville = :ville AND now() < start_formation order by id_form DESC';
    $tab_form = $bdd->prepare($queryTypeLieu);
    $tab_form->bindValue(':ville', $_POST["Lieu"]);
    $tab_form->execute();
    $QLieu = $tab_form->fetchAll(PDO::FETCH_ASSOC);
    ?>
        <table class="events-table">
            <thead>
                <tr>
                    <th class="event-intitule">Intitulé</th>
                    <th class="event-type">Formation</th>
                    <th class="event-ville">Ville</th>
                    <th class="event-date">Date</th>
                </tr>
            </thead>
            <?php
            $i = 0;
            foreach ($QLieu as $base => $val) {
                if ($i % 2) { ?>
                    <tr class="colorRes">
                    <?php
                } else {
                    ?>
                    <tr class="">
                    <?php
                }
                    ?>
                     <th id="Nom"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["nom"]; ?></a></th>
                        <th id="Type"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["type"]; ?></a></th>
                        <th id="Ville"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["ville"]; ?></a></th>
                        <th id="Date"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= date_convert("Le", $val["start_formation"]); ?></a></th>
                    </tr>
                <?php
                $i++;
            }
                ?>
        </table>
    <?php
} elseif (!empty($_POST["Formation"]) && empty($_POST["Lieu"])) { //Si on filtre seulement les types de formations
    $queryTypeLieu .= ' WHERE type = :type AND now() < start_formation order by id_form DESC';
    $tab_form = $bdd->prepare($queryTypeLieu);
    $tab_form->bindValue(':type', $_POST["Formation"]);
    $tab_form->execute();
    $QType = $tab_form->fetchAll(PDO::FETCH_ASSOC);
    ?>
        <table class="events-table">
            <thead>
                <tr>
                    <th class="event-intitule">Intitulé</th>
                    <th class="event-type">Formation</th>
                    <th class="event-ville">Ville</th>
                    <th class="event-date">Date</th>
                </tr>
            </thead>
            <?php
            $i = 0;
            foreach ($QType as $base => $val) {
                if ($i % 2) { ?>
                    <tr class="colorRes">
                    <?php
                } else {
                    ?>
                    <tr class="">
                    <?php
                }
                    ?>
                     <th id="Nom"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["nom"]; ?></a></th>
                        <th id="Type"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["type"]; ?></a></th>
                        <th id="Ville"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["ville"]; ?></a></th>
                        <th id="Date"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= date_convert("Le",$val["start_formation"]); ?></a></th>
                    </tr>
                <?php
                $i++;
            }
                ?>
        </table>
    <?php
} elseif (empty($_POST["Formation"]) && empty($_POST["Lieu"])) { //Si les selects = vide
    $queryTypeLieu .= ' WHERE now() < start_formation order by id_form DESC';
    $tab_form = $bdd->prepare($queryTypeLieu);
    $tab_form->execute();
    $Alltab = $tab_form->fetchAll(PDO::FETCH_ASSOC);
    ?>
        <table class="events-table">
            <thead>
                <tr>
                    <th class="event-intitule">Intitulé</th>
                    <th class="event-type">Formation</th>
                    <th class="event-ville">Ville</th>
                    <th class="event-date">Date</th>
                </tr>
            </thead>
            <?php
            $i = 0;
            foreach ($Alltab as $base => $val) {
                if ($i % 2) { ?>
                    <tr class="colorRes">
                    <?php
                } else {
                    ?>
                    <tr class="">
                    <?php
                }
                    ?>
                    <th id="Nom"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["nom"]; ?></a></th>
                        <th id="Type"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["type"]; ?></a></th>
                        <th id="Lieu"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= $val["ville"]; ?></a></th>
                        <th id="Date"><a class="normal-a" href="../fr/registration.php?idform=<?= $val["id_form"];?>"><?= date_convert("Le", $val["start_formation"]); ?></a></th>
                    </tr>
                <?php
                $i++;
            }
                ?>
        </table>
    <?php
}

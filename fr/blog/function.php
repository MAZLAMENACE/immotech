<?php 

// function qui récupère tous les articles
function getArticles(){
	$bdd = get_pdo();
	$stat = $bdd->prepare('SELECT id, titre, contenu, dateajout, image FROM news ORDER BY id DESC');
	$stat->execute();
	$data = $stat->fetchAll(PDO::FETCH_OBJ);
	return $data;
	$stat->closeCursor();
}
// function qui récupère un article
function getArticle($id){
	$bdd = get_pdo();
	$stat = $bdd->prepare('SELECT * FROM news WHERE id = ?');
	$stat->execute(array($id));
	if ($stat->rowCount() == 1){
		$data = $stat->fetch(PDO::FETCH_OBJ);
		return $data;
	}
	else{
		e404();
	}
}

?> 

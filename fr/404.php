<?php


http_response_code(404);

include_once'../include/header.inc';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../CSS/font_style.css">
    <link rel="stylesheet" href="../../CSS/variables_style.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/header.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/responsive.css">
    <title>Erreur 404</title>
</head>

<body>
    <div class="section404">
        <h1>Erreur 404 ! La page que vous recherchez n'existe pas...</h1>
    </div>
</body>

</html>
<?php 
include_once'../include/footer.inc';

?>
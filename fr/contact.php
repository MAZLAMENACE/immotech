<?php
session_start();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../functPHP/PHPMailer-master/src/Exception.php';
require '../functPHP/PHPMailer-master/src/PHPMailer.php';
require '../functPHP/PHPMailer-master/src/SMTP.php';
require "./src/help.php";

// empecher le spam formulaire
if (isset($_SESSION['contact']) AND $_SESSION['contact'] == "pass"){
  $_SESSION['contact'] = "autorise" ;
  goToIndex();
}
$_SESSION['contact'] = "autorise" ;

$bdd = get_pdo();
$requete = $bdd->prepare('SELECT email from administrateur');
$requeteisok = $requete->execute();
foreach ($requete as $value) {
  $emailenv = $value['email'];
}

if (isset($_POST['Envoie']) AND !empty($_POST['Envoie'])) {
  // en cas de bot
  if (isset($_POST['honey']) AND !empty($_POST['honey'])){
    goToIndex();
    exit;
  }
  $_POST['email'] = isset($_POST['email']) ? $_POST['email'] : 'aa@gmail.com';
  if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i", htmlspecialchars($_POST['email']))) {
    $mail = new PHPMailer(true);

    try {
      //Server settings
      $mail->SMTPDebug = 0;
      $mail->isSMTP();
      $mail->Host       = 'tls://smtp.gmail.com';
      $mail->SMTPAuth   = true;
      $mail->Username   = 'dg8125791@gmail.com';
      $mail->Password   = 'maile1234';
      $mail->SMTPSecure =  'tls';
      $mail->Port       = 587;

      $prenom = htmlspecialchars($_POST['prenom']);
      $nom = htmlspecialchars($_POST['nom']);
      $adresseM = htmlspecialchars($_POST['email']);
      $sujet = htmlspecialchars($_POST['sujet']);
      $message = htmlspecialchars($_POST['message']);

      //Recipients

      $mail->setFrom('dg8125791@gmail.com', 'Demande de contact pour : ' . $prenom);
      $mail->addAddress($emailenv, 'George Delphine');
      $subject = 'Formulaire de contact ' . $prenom . ' ' . $nom;
      $message = '<h2>Bonjour, je m\'appelle ' . $prenom . ' ' . $nom . '.</h2> <br> ' .
        '<b>Je souhaiterais plus de renseignements sur : </b> ' . $sujet . '<br>' .
        '<b>Mon message : </b>' . $message . '<br>' . '<br>' .
        '<b>Mon adresse mail</b> : ' . $adresseM . ' <br>' .
        // Content
        $mail->isHTML(true);
      $mail->Subject = $subject;
      $mail->Body    = $message;
      // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

      $mail->send();
      status("Votre message a bien été envoyé ! Une réponse vous sera donnée prochainement.");
      $_SESSION['contact'] = "pass";
      header("Refresh:5; url =/");
  
    } catch (Exception $e) {
      status('L\'adresse email n\'est pas configuré',"error");
    }
  } else {
    status('On dirait bien que votre adresse mail est incorrect...',"error");
  }
}
// ne pas oublier de désactiver la sécurité sur le compte google
?>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v7.0&appId=544223183124673&autoLogAppEvents=1" nonce="elijqda1"></script>
<?php include '../include/header.inc';?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="../CSS/font_style.css" />
  <link rel="stylesheet" href="../CSS/variables_style.css" />
  <link rel="stylesheet" href="../CSS/responsive.css" />
  <link rel="stylesheet" href="../CSS/actualite.css" />
  <link rel="stylesheet" type="text/css" href="../CSS/contact.css" />
  <link rel="stylesheet" type="text/css" href="../CSS/index.css">
  <title>Contact</title>
</head>
<body>
  <div id="container_entete_contact">
    <div id="titre_entete_contact">
      <h1 class="t-white">
        Les contacts
      </h1>
    </div>
  </div>
  <main>
    <h2 class="centrer ent">Qui <strong>suis-je ?</strong></h2>
    <div id="box_quisuisje" >
      <div id="text_quisuisje" class="m-auto">
        <h3 id="title_box">Mon <strong>expérience</strong></h3>
        <br>
        <div class="delay">
        <p >Mon objectif : Accompagner chacun dans sa réussite professionnelle et la mise en place d’une organisation fiable et motivante.</p>
        <p>Spécialiste de la gestion immobilière depuis près de 15 ans, ma mission est d’aider les professionnels de l’immobilier à travailler plus sereinement.</p><br>
        <p>Experte dans le domaine de la copropriété, j’interviens auprès des agences pour les accompagner vers une meilleure compréhension de leurs nouvelles obligations.</p><br>
        <p><em>Mes objectifs</em> sont :</p><br>
        <p>&#10140 de vous apporter toutes les clés pour maîtriser vos obligations et les mettre en œuvre efficacement.</p>
        <p>&#10140 et dynamiser vos équipes pour les fidéliser par leur épanouissement quotidien dans leur poste de travail.</p>
        </div>
        <div class="fb-share-button disp" data-href="https://www.facebook.com/depecheSyndic7/" data-layout="button">
        </div>
      </div>
      <div id="box_prestataire">
        <img src="../annex/photo/commanditaire.jpg" class="img_quisuisje delay">
      </div>
    </div>
    <h2 class="centrer ent">Une question ? Une demande ? <strong>Contactez moi !</strong></h2>
    <div id="formulaire_contact">
      <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
        <img src="../annex/images/formulaire.svg">
        <label>Nom</label>
       
        <input type="text" name="nom" required  <?php if(isset($_POST['nom'])) { echo "value='".$_POST['nom']."'"; } ?>>
        <label>Prénom</label>
        <input type="text" name="prenom" required <?php if(isset($_POST['prenom'])) { echo "value='".$_POST['prenom']."'"; } ?>>
        <label>Email</label>
        <input type="text" name="email" required <?php if(isset($_POST['email'])) { echo "value='".$_POST['email']."'"; } ?>>
        <label>Sujet</label>
        <input type="text" name="sujet" required <?php if(isset($_POST['sujet'])) { echo "value='".$_POST['sujet']."'"; } ?>>
        <input type="text*" name="honey" id="honey">
        <label>Votre message</label>
        <textarea type="text" name="message" required ><?php if(isset($_POST['message'])) { echo $_POST['message']; } ?></textarea>
        <input type="submit" name="Envoie" class="submit_formulaire" required>
      </form>
    </div>
  </main>
</body>

</html>
<script src="https://unpkg.com/scrollreveal"></script>
<script type="text/javascript" src="../JS/interaction.js"></script>

<?php

include '../include/footer.inc';
?>
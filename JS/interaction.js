// API REVEAL

const sr = ScrollReveal({
	duration:1000, 
});
sr.reveal('.poup', {
	duration:4000,
	scale: 0.8
});


sr.reveal('.translate', {
	distance: '100px',
	origin : 'left'
});

sr.reveal('.delay', {
	delay: 800,
	interval:500
});

sr.reveal('.spot', {
	delay :2000
});

sr.reveal('.spot2', {
	delay: 300

});

function switsh(val){
	switch (val) {
		case 'activity':
			document.getElementById("immobil").style.display= "none";
			document.getElementById("immobil2").style.display= "flex";
			break;
		case 'elan':
			document.getElementById("loielan").style.display= "none";
			document.getElementById("loielan2").style.display= "flex";
			break;
		case 'conflict':
			document.getElementById("prevconflict").style.display= "none";
			document.getElementById("prevconflict2").style.display= "flex";
			break;
		case 'rgpd':
			document.getElementById("conformiter").style.display= "none";
			document.getElementById("conformiter2").style.display= "flex";
			break;
		default:
			break;
	}

};

function switshoff(val){
	switch (val) {
		case 'activity':
			document.getElementById("immobil2").style.display= "none";
			document.getElementById("immobil").style.display= "flex";
			break;
		case 'elan':
			document.getElementById("loielan2").style.display= "none";
			document.getElementById("loielan").style.display= "flex";
			break;
		case 'conflict':
			document.getElementById("prevconflict2").style.display= "none";
			document.getElementById("prevconflict").style.display= "flex";
			break;
		case 'rgpd':
			document.getElementById("conformiter2").style.display= "none";
			document.getElementById("conformiter").style.display= "flex";
			break;
		default:
			break;
	}
};

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="../CSS/footer.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/variable_style.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/responsive.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/font_style.css" />
  </head>
  <body>
    <!-- Création du footer général pour toute les pages -->
    <footer>
      <!--Création d'un "about us"-->
      <div id="aboutus">
        <h3 class="centrer white">Mentions légales</h3>
        <p class="j">
          N° Siret 834 118 713 00017 RCS Toulouse / Code APE 8559A / N° TVA Intracom. FR 9834118713 / SASU au capital social de 1 000 euros / NDA : 76310919431 (cet enregistrement ne vaut pas agrément de l’état).
        </p>
      </div>
      <!--Création d'un emplacement "liens rapides"-->
      <div id="quicklinks">
        <h3 class="centrer white">Liens utiles</h3>
        <p>
          <a href="../fr/cgv.php" class="white-button">
          Conditions générales de ventes
          </a>
        </p>
        <p>
          <a href="../fr/rgpd.php" class="white-button">
          Charte protection des données
          </a>
        </p>
        <br>
        <div class="rzo">
        <!-- facebook -->
        <a href="https://www.facebook.com/depecheSyndic7/">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><style type="text/css">  
          .fb-color{fill:#4267b2;}
          .bikoblanco{fill:#FFFFFF;}
          </style><path class="fb-color" d="M483.7 0H28.2C12.6 0 0 12.7 0 28.3v455.5C0 499.4 12.7 512 28.3 512h455.5c15.6 0 28.3-12.6 28.3-28.3 0 0 0 0 0 0V28.2C512 12.6 499.3 0 483.7 0z"/><path class="bikoblanco" d="M353.5 512V314h66.8l10-77.5h-76.8v-49.4c0-22.4 6.2-37.6 38.3-37.6h40.7V80.4c-7.1-0.9-31.4-3-59.6-3 -59 0-99.4 36-99.4 102.1v57H207V314h66.5v198H353.5z"/>
          </svg>
        </a>
        <!-- linkedn -->
        <a href="https://www.linkedin.com/in/vivianelabbe/">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 455.7 455.7"><style type="text/css">
          .ln-color{fill:#0084B1;}
          .bikoblanco{fill:#FFFFFF;}
          </style><path class="ln-color" d="M430.6 0H25.1C11.3 0 0 11.3 0 25.2v405.4c0 13.9 11.3 25.1 25.2 25.1h405.4c13.9 0 25.2-11.3 25.2-25.1 0 0 0 0 0 0V25.1C455.7 11.3 444.5 0 430.6 0z"/><path class="bikoblanco" d="M107.3 69.2c20.9 0 38.1 17.3 38 38.2 -0.1 22-18.3 38.5-38.3 38 -20.3 0.4-38.2-16.6-38.2-38.2C68.8 86.3 86.1 69.2 107.3 69.2z"/><path class="bikoblanco" d="M129.4 386.5H84.7c-5.8 0-10.5-4.7-10.5-10.5V185.2c0-5.8 4.7-10.5 10.5-10.5h44.7c5.8 0 10.5 4.7 10.5 10.5V376C139.9 381.8 135.2 386.5 129.4 386.5z"/><path class="bikoblanco" d="M386.9 241.7c0-40-32.4-72.4-72.4-72.4H303c-21.9 0-41.2 10.9-52.8 27.6 -1.3 1.8-2.4 3.7-3.5 5.7 -0.4-0.1-0.6-0.1-0.6-0.1V179c0-2.4-2-4.4-4.4-4.4h-55.8c-2.4 0-4.4 2-4.4 4.4v203.1c0 2.4 2 4.4 4.4 4.4l57 0c2.4 0 4.4-2 4.4-4.4V264.8c0-20.3 16.2-37.1 36.5-37.3 10.4-0.1 19.7 4 26.5 10.8 6.7 6.7 10.8 15.9 10.8 26.1v117.8c0 2.4 2 4.4 4.4 4.4l57.2 0c2.4 0 4.4-2 4.4-4.4L386.9 241.7 386.9 241.7z"/></svg>
        </a>
        <!-- twitter -->
        <a href="https://twitter.com/vivianelabbe">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 450.1 455.7"><style type="text/css">
          .tw-color{fill:#50ABF1;}
          .bikoblanco{fill:#FFFFFF;}
          </style><path class="tw-color" d="M430.6 0H25.1C11.3 0 0 11.3 0 25.2v405.4c0 13.9 11.3 25.1 25.2 25.1h405.4c13.9 0 25.2-11.3 25.2-25.1 0 0 0 0 0 0V25.1C455.7 11.3 444.5 0 430.6 0z"/><path class="bikoblanco" d="M393.1 140.5c-11.8 5.2-24.3 8.6-37.4 10.2 13.4-8 23.7-20.6 28.5-35.8 -12.5 7.5-26.4 12.8-41.1 15.7 -11.9-12.7-28.9-20.5-47.4-20.5 -35.9 0-64.8 29.1-64.8 64.9 0 5.1 0.4 10.1 1.5 14.8 -53.9-2.6-101.6-28.5-133.7-67.8 -5.6 9.7-8.9 20.8-8.9 32.8 0 22.5 11.6 42.4 28.8 53.9 -10.4-0.2-20.6-3.2-29.3-8 0 0.2 0 0.5 0 0.7 0 31.5 22.5 57.7 52 63.7 -5.3 1.4-11 2.1-17 2.1 -4.2 0-8.3-0.2-12.3-1.1 8.4 25.7 32.2 44.6 60.6 45.2 -22.1 17.3-50.1 27.7-80.4 27.7 -5.3 0-10.4-0.2-15.5-0.9 28.7 18.5 62.8 29.1 99.5 29.1 119.3 0 184.6-98.9 184.6-184.6 0-2.9-0.1-5.6-0.2-8.4C373.5 165 384.3 153.6 393.1 140.5z"/></svg>
        </a>
      </div>
      </div>
      <!--Signature de l'équipe-->
      <div id="created_by">
        <h3 class="centrer white">Créé par</h3>
        <p><em>Clément MAZEAUD</em> - Chef de projet</p>
        <p><em>Vincent DUCOURNAU</em> - Développeur</p>
        <p><em>Edgar BUCHS</em> - Développeur</p>
        <div class="centrer">
        <a href="https://www.intechinfo.fr/"><img  src="../annex/photo/logo_intech.PNG" alt="logo d'intech sud"></a>
        </div>
      </div>
    </footer>
  </body>
</html>

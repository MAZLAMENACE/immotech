<html>
  <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="../CSS/header.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/variables_style.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/responsive.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/font_style.css" />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <script type="text/javascript" src="../JS/menu_burger.js"></script>
  </head>
  <header>
    <div id="box_logo">
    <a href="/" class="normal-a">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 353.6 361" id="logo">
        <style type="text/css">
          .st0 {
            fill: white;
          }
          .st1 {
            fill: #ef3f42;
          }
        </style>
        <path
          class="st0"
          d="M176.8 291c7.9-5.7 18.3-12.2 31-18.4V205h-64v66.6C157.4 278.1 168.4 285 176.8 291z"
        />
        <path
          class="st1"
          d="M288.4 126c0.5 9.2 0.9 18.4 1.4 27.7l-118-117.9 13.4-13.5C219.6 56.9 254 91.4 288.4 126z"
        />
        <path
          class="st0"
          d="M60.3 142.1c-0.6 9.2-1.2 18.4-1.9 27.6L178.4 54l-13.2-13.7C130.2 74.3 95.3 108.2 60.3 142.1z"
        />
        <path
          class="st1"
          d="M82.3 156.1c5.5-4.7 11-9.4 16.5-14.1l0.8 114c-5.7 0-11.3 0-17 0C82.5 222.7 82.4 189.4 82.3 156.1z"
        />
        <path
          class="st0"
          d="M269.4 144.8c-5.5-5.3-11-10.6-16.5-15.9L252 257c5.7 0 11.3 0 17 0C269.1 219.6 269.3 182.2 269.4 144.8z"
        />
        <path
          class="st1"
          d="M262.9 305.2l12.7-9.2 12.2 9.2V281H263C263 289.1 262.9 297.1 262.9 305.2z"
        />
        <path
          class="st0"
          d="M40.8 251c17.7-0.8 44.8 0.1 76 10 26.6 8.5 46.7 20.5 60 30 0 1.7 0 3.3 0 5 -13.7-10.1-35.9-24-66-33 -28.5-8.6-53.4-9.6-70-9C40.8 253 40.8 252 40.8 251z"
        />
        <path
          class="st0"
          d="M312.8 251c-17.7-0.8-44.8 0.1-76 10 -26.6 8.5-46.7 20.5-60 30 0 1.7 0 3.3 0 5 13.7-10.1 35.9-24 66-33 28.5-8.6 53.4-9.6 70-9C312.8 253 312.8 252 312.8 251z"
        />
        <rect x="151.6" y="158.3" class="st0" width="23" height="23.1" />
        <rect x="179.8" y="158.4" class="st0" width="23" height="23.1" />
        <rect x="179.7" y="127.3" class="st0" width="23" height="23.1" />
        <rect x="151.5" y="127.4" class="st0" width="23" height="23.1" />
        <path
          class="st0"
          d="M176.8 303.4c0 10.9 0 21.8 0 32.7 -6.1-6.7-15.6-16-28.9-24.6 -20.7-13.3-39.7-17.9-55.6-21.5 -12.6-2.9-30.3-5.8-51.9-5.7 0-8.3 0-16.5 0.1-24.8 17-0.1 31.5 1.6 42.8 3.5 14.7 2.5 35.3 6 58.9 17.8C157.1 288.2 168.6 296.6 176.8 303.4z"
        />
        <path
          class="st0"
          d="M176.8 303.4c0 10.9 0 21.8 0 32.7 6.1-6.7 15.6-16 28.9-24.6 20.7-13.3 39.7-17.9 55.6-21.5 12.6-2.9 30.3-5.8 51.9-5.7 0-8.3 0-16.5-0.1-24.8 -17-0.1-31.5 1.6-42.8 3.5 -14.7 2.5-35.3 6-58.9 17.8C196.5 288.2 185 296.6 176.8 303.4z"
        />
      </svg>

    </a>
      <a href="/" class="normal-a"><p id="nom_entreprise" >La Dépêche du Syndic</p><a>
    </div>

    <div class="box_burger" onclick="vasyouvretoibb()">
      <div id="icone_burger">&#9776;</div>
    </div>
    <div class="mainMenu">
      <!-- menu ordinateur -->
      <a href="/">Accueil</a>
      <a href="../fr/actualites.php">Actualités</a>
      <a href="../fr/contact.php">Contact</a>
      <a href="../fr/formations.php" class="btn-formation">Formations</a>
    </div>
  </header>
</html>

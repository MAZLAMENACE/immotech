<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="/CSS/admin/header_admin.css">
		<link rel="stylesheet" type="text/css" href="/CSS/font_style.css">
		<link rel="stylesheet" type="text/css" href="/CSS/variables_style.css">
	</head>
		<header>
			<a href="/fr/admin/agenda/modif_agenda.php"><div><img src="/annex/images/agenda.svg" class ="taille_sigle"> </div></a>
			<a href="/fr/admin/blog/modif_blog.php"><div><img src="/annex/images/blog.svg"class ="taille_sigle" ></div></a>
			<a href="/fr/admin/edit/modif_site.php"><div><img src="/annex/images/site.svg" class ="taille_sigle"></div></a>	
			<a id=retour_acc href="../../admin/deconnexion.php" class="taille_sigle">
        			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M298.7 138.7c0 58.9-47.8 106.7-106.7 106.7S85.3 197.6 85.3 138.7 133.1 32 192 32 298.7 79.8 298.7 138.7z"/><path d="M282.7 288H101.3C45.5 288 0 333.5 0 389.3V464c0 8.8 7.2 16 16 16h352c8.8 0 16-7.2 16-16v-74.7C384 333.5 338.5 288 282.7 288z"/><path d="M506.9 212.3l-74.7-69.3c-3-2.8-6.9-4.2-10.9-4.2 -10.6 0-16 9-16 16v48H320c-11.8 0-21.3 9.6-21.3 21.3s9.5 21.3 21.3 21.3h85.3v48c0 8.9 7.2 16 16 16 4 0 7.9-1.5 10.9-4.3l74.7-69.3c3.2-3 5.1-7.3 5.1-11.7S510.1 215.3 506.9 212.3z"	/></svg>
     
    		</a>
		</header>
</html>
 <?php
 session_start();

  include_once 'include/header.inc';
  require 'fr/blog/function.php';
  require_once 'fr/src/help.php';

  $bdd = get_pdo();
  $req = $bdd->prepare('SELECT * FROM news ORDER BY dateajout DESC LIMIT 1');
  $req->execute();
  $lastArticle = $req->fetch();

  ?>

 <!DOCTYPE html>
 <html lang="fr">

 <head>
   <title>Accueil</title>
   <meta charset="utf-8">
   <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
   <link rel="stylesheet" type="text/css" href="CSS/index.css">
   <link rel="stylesheet" type="text/css" href="CSS/variable_style.css" />
   <link rel="stylesheet" type="text/css" href="CSS/font_style.css" />
   <link rel="stylesheet" type="text/css" href="CSS/responsive.css" />
 </head>

 <body>
   <div id="imageacc">
     <div class="container-titre">
       <h1 class="poup" id="titre">Expert en formation immobilière</h1>
       <a href="fr/formations.php" class="normal-a spot">Trouver ma formation !</a>
     </div>
   </div>
   <main>
     <div id="lastactu">
       <div>
         <h2 class="centrer ent">La dernière <strong>actualité</strong></h2>
       </div>
      <?php if (isset($lastArticle['titre']) AND !empty($lastArticle['titre'])): ?>
       <div class="section disp translate aer">
         <div>
           <h3><?= $lastArticle['titre'] ?></h3>
           <p id="lastarticle" class="j"><?= strip_tags(substr($lastArticle['contenu'], 0, 300) . "..."); ?>
             <a href="fr/article.php?id=<?= $lastArticle['id'] ?>">Lire la suite</a></p>
         </div>
         <div class="box-img">
           <img src="fr/admin/upload/<?= $lastArticle['image'] ?>" alt="" class="tailleimg poup" />
         </div>
       </div>
      <?php else :?>
        <div class="section disp translate">
          <p class="centrer">Aucun article n'a été posté pour le moment... Soyez patient !</p>
        </div>
     <?php endif ?>
       <div id="descriptionentreprise">
         <h2 class="centrer ent">Qu'est ce que la <strong>Dépêche du Syndic ?</strong></h2>
         <div class="disp poup m-auto">
           <p>
             Accompagner les professionnels de l’immobilier dans les changements législatifs et les aider dans leur évolution vers des pratiques nouvelles sont les deux missions fondatrices de La Dépêche du Syndic depuis maintenant plusieurs années, le tout dans un esprit d’échange et de partage d’expérience.
           </p><br>
           <p>Organisme de formation, référencé au datadock, spécialisé dans les domaines de l’immobilier et de l’organisation professionnelle, la Dépêche du Syndic :</p>
           <p class="m10">&#10140 Vous forme en respectant les exigences du Décret n° 2016-173 pour le renouvellement de la carte professionnelle.</p>
           <p class="m10">&#10140 Vous garantit une gestion immédiatement efficace par vos collaborateurs.</p>
           <p class="m10">&#10140 Vous aide à conforter votre image de professionnel responsable.</p>
           <p class="m10">&#10140 Vous permet d’assurer une prise en charge éclairée de la gestion des risques.</p>
         </div>
         <img id="img-entreprise" class="poup" src="https://cdn.pixabay.com/photo/2018/03/10/12/00/paper-3213924_1280.jpg">
       </div>
       <div id="box_prestation">
         <div id="actu" class="delay">
           <div>
             <a href="fr/actualites.php"><img src="../annex/images/news.svg" alt="" class="sigle" /></a>
           </div>
           <div>
             <h3 class="centrer">Actualités</h3>
             <p>
               Des infos, des actus, des anecdotes à lire sans modération !
             </p>
           </div>
         </div>
         <div id="formation" class="delay">
           <div>
             <a href="fr/formations.php"><img src="../annex/images/formation.svg" alt="" class="sigle" /></a>
           </div>
           <div>
             <h3 class="centrer">Formation</h3>
             <p>
               Des programmes de formation clé en main adaptés et renouvelés avec les évolutions législatives. Et la possibilité de bâtir son programme personnalisé à la carte correspondant aux attentes de vos équipes.
             </p>
           </div>
         </div>
         <div id="contact" class="delay">
           <div>
             <a href="fr/contact.php"><img src="../annex/images/contactus.svg" alt="" class="sigle" /></a>
           </div>
           <div>
             <h3 class="centrer">Contact</h3>
             <p>
               Pour toute demande, retrouvez toutes mes coordonnées de contact en cliquant ici !
             </p>
           </div>
         </div>
       </div>
   </main>
 </body>

 </html>
 <script src="https://unpkg.com/scrollreveal"></script>
 <script type="text/javascript" src="JS/interaction.js"></script>

 <?php
  include 'include/footer.inc';
  ?>